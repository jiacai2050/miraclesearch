<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>关于我们</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/index.css" rel="stylesheet" type="text/css" />
		<!--
	    <link rel="stylesheet" type="text/css" href="styles.css">
	    -->

	</head>

	<body>
		<table width="403px" height="120px" cellpadding="0" cellspacing="0"
			align="center">
			<tr>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<table width="403" height="203" border="1" cellpadding="0"
			cellspacing="0" align="center">
			<caption>
				<h2>
					<strong>关于我们</strong>
				</h2>
			</caption>
			<tr>
				<th width="98" scope="col">
					队名
				</th>
				<th width="299" scope="col">
					Miracle
				</th>
			</tr>
			<tr>
				<th scope="row">
					队员
				</th>
				<td>
					刘家财、刘华、刘雨、韩玉敏、孙秀林
				</td>
			</tr>
			<tr>
				<th scope="row">
					队长
				</th>
				<td>
					刘家财
				</td>
			</tr>
			<tr>
				<th scope="row">
					学校
				</th>
				<td>
					<a href="http://www.ytu.edu.cn" target="_blank">烟台大学&nbsp;&nbsp;<img
							src="images/ytu.jpg" height="28px" width="100px" /> </a>
				</td>
			</tr>
			<tr>
				<th scope="row">
					指导老师
				</th>
				<td>
					刘其成
				</td>
			</tr>
			<tr>
				<th scope="row">
					联系方式
				</th>
				<td>
					<a href="mailto:liujiacai@163.com">liujiacai@163.com</a>
				</td>
			</tr>
		</table>
	</body>
</html>
