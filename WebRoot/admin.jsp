<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>后台管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/login-admin.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/admin.js"></script>
	</head>

	<body class="admin-body">
		<div style="height: 20px"></div>
		<table cellSpacing="0" cellPadding="0" width="100%">
			<tr height="56px">
				<td width="200px">
					<img height="85px" src="images/logo.gif" width="200px">
				</td>
				<td
					style="font-weight: bold; color: #000; padding-top: 50px; font-size: 16px"
					align=center>
					当前用户：<s:property value="username"/> &nbsp;&nbsp;
					<a style="COLOR: #000"
						onclick="if (confirm('确定要退出吗？')) return true; else return false;"
						href="index.jsp" target=_top>退出系统</a>
				</td>
			</tr>
		</table>

		<marquee>
			<font color="red" size="5px"> 后台管理系统</font>
		</marquee>
		<br />
		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>

		<div id="resources_div">
			<div class="title">
				资源上传
			</div>
			<br>	<br>	<br>
			<center>
			点击
			<a href="upload.jsp" target="_blank">这里</a>上传资源
			</center>
		</div>
		<div id="word_div">
			<div class="title">
				词库管理
			</div>
			现在有的专有名词：
			<div id="noun_content">

			</div>
			<br>
			添加新的专有名词
			<br>
			<input type="text" id="newDic">
			<input type="button" name="myNoun" onclick="submitDic(this)"
				value="提交">
		</div>
		<div id="stop_div">
			<div class="title">
				停词管理
			</div>
			现在有的禁用词：
			<div id="stop_content">

			</div>
			<br>
			添加新的停用词
			<br>
			<input type="text" id="newDic">
			<input type="button" name="myStop" onclick="submitDic(this)"
				value="提交">
		</div>

		<table height="100%" cellSpacing="0" cellPadding="0" width="205px"
			background=images/admin_bg.jpg>
			<tr>
				<td vAlign=top align="center">
					<table cellSpacing="0" cellPadding="0" width="100%">
						<tr>
							<td height=10></td>
						</tr>
					</table>
					<table cellSpacing="0" cellPadding="0" width="185px">
						<tr height="30px">
							<td style="padding-left: 50px" background=images/admin_bt.jpg>
								<a class=menuParent onclick=expand(1) href="javascript:void(0);">
									资源管理 </a>
							</td>
						</tr>
						<tr height="4px">
							<td></td>
						</tr>
					</table>
					<table id=child1 style="display: none" cellSpacing="0"
						cellPadding="0" width="150px">
						<tr height="30px">
							<td align="center" width="30px">
								<img height="9px" width="9px" src="images/admin_icon.gif">
							</td>
							<td>
								<a class=menuChild onclick="show_res_div()">资源上传</a>
							</td>
						</tr>
						<tr height="4px">
							<td colSPan=2></td>
						</tr>
					</table>
					<table cellSpacing="0" cellPadding="0" width="185px">
						<tr height="30px">
							<td style="padding-left: 50px" background=images/admin_bt.jpg>
								<a class=menuParent onclick=expand(2) href="javascript:void(0);">
									系统设置 </a>
							</td>
						</tr>
						<tr height="4px">
							<td></td>
						</tr>
					</table>
					<table id=child2 style="display: none" cellSpacing="0"
						cellPadding="0" width="150px">
						<tr height="30px">
							<td align="center" width="30px">
								<img height="9px" width="9px" src="images/admin_icon.gif">
							</td>
							<td>
								<a class=menuChild onclick="show_word_div()">词库管理</a>
							</td>
						</tr>
						<tr height="30px">
							<td align="center" width=30>
								<img height="9px" width="9px" src="images/admin_icon.gif">
							</td>
							<td>
								<a class=menuChild onclick="show_stop_div()">停词管理</a>
							</td>
						</tr>
						<tr height="4px">
							<td></td>
						</tr>
					</table>
	</body>
</html>
