<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>搜索结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/result.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript">
        $(function() {
	         $("#nav a").each(function() {
		     $(this).addClass("navigations");
	         });

	         $("#nav a[name=<s:property value='type'/>]").removeClass("navigations");
	         $("#nav a[name=<s:property value='type'/>]").addClass("current");

        });
        </script>
	</head>

	<body>
		<div id="nav">
			<a name="all"
				href="Search.action?keyword=<s:property value='keyword'/>&type=all&pageIndex=1">全部</a>|
			<a name="document"
				href="Search.action?keyword=<s:property value='keyword'/>&type=document&pageIndex=1">文档</a>|
			<a name="image"
				href="Search.action?keyword=<s:property value='keyword'/>&type=image&pageIndex=1">图片</a>|
			<a name="music"
				href="Search.action?keyword=<s:property value='keyword'/>&type=music&pageIndex=1">音频</a>|
			<a name="video"
				href="Search.action?keyword=<s:property value='keyword'/>&type=video&pageIndex=1">视频</a>|
			<a name="other"
				href="Search.action?keyword=<s:property value='keyword'/>&type=other&pageIndex=1">其他</a>
			<div id="login">
				<a href="index.jsp">网站首页</a>|
				<a href="login.jsp">登录</a>
			</div>
		</div>
		<!--nav over -->
		<div id="Search_top">
			<br />
			<div>
				<img class="img1" src="images/logo.gif" />
			</div>
			<form action="Search.action" method="get">
				<div>
					<input type="text" class="keyword" name="keyword" id="keyword"
						autocomplete="off" value="<s:property value='keyword'/>" />
					<input type="submit" style="border: 0px" value=""
						onClick="return checkValid();" class="search_btn" id="search_btn" />
					<input type="hidden" name="type" value="all">
				</div>
			</form>
		</div>
		<div class="search_time">
			搜索到
			<s:property value="resultBean.length" />
			项结果，用时
			<s:property value="resultBean.time" />
			ms
		</div>
		<!--Search_top  over-->

		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>
	<%
			String exec = (String) request.getAttribute("calc_expression");
			String result = (String) request.getAttribute("calc_result");
			if (exec != null) {
				out.print(exec + "=" + result);
				out.print("&nbsp;" + "<a target='_blank' href='calc.html'>展开智能计算器</a>");
			}
			String expand = (String) request.getAttribute("expand");
			if(expand != "") {
				out.print(expand + "<br>");		
			}
			
		%>
		<div id="mainBody" class="content">
			<br />
			抱歉，没有找到与
			<font color="#FF0000"><s:property value="keyword" /> </font>相关的内容。
			<br />
			建议：
			<br />
			<ul>
				<li>
					请检查输入字词有无错误。
				</li>
				<li>
					请尝试其他的查询词。
				</li>
				<li>
					请改用较常见的字词。
				</li>
			</ul>
		</div>
		<br />
		<br />
		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>
		<br />
		<form action="Search.action" method="get">
			<div id="Search_bottom">
				<input type="text" class="keyword_bottom" name="keyword"
					id="keyword" autocomplete="off"
					value="<s:property value='keyword'/>" />
				<input type="submit" style="border: 0px" value=""
					onClick="return checkValid();" class="search_btn_bottom"
					id="search_btn" />
				<input type="hidden" name="type" value="all">
			</div>
		</form>

		<br />
		<br />
		<br />
		<br />
		<p class="CR_p">
			&copy 2012 Miracle Workgroup|
			<a target="_blank" href="aboutUS.jsp">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</p>
	</body>
</html>
