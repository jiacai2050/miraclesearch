<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>文档预览</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/result.css" rel="stylesheet" type="text/css" />	
		<script type="text/javascript" src="js/swfobject.js"></script>
		<script type="text/javascript" src="js/flexpaper_flash.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript">
		$(function() {
			
			$("#nav a").each(function() {
				$(this).addClass("navigations");
			});
			
			$("#nav a[name=<s:property value='type'/>]").removeClass("navigations");
			$("#nav a[name=<s:property value='type'/>]").addClass("current");
	
		});		
		</script>
	</head>
	
	<body>
		<div id="nav">
			<a name="all"
				href="Search.action?keyword=<s:property value='keyword'/>&type=all&pageIndex=1">全部</a>|
			<a name="document"
				href="Search.action?keyword=<s:property value='keyword'/>&type=document&pageIndex=1">文档</a>|
			<a name="image"
				href="Search.action?keyword=<s:property value='keyword'/>&type=image&pageIndex=1">图片</a>|
			<a name="music"
				href="Search.action?keyword=<s:property value='keyword'/>&type=music&pageIndex=1">音频</a>|
			<a name="video"
				href="Search.action?keyword=<s:property value='keyword'/>&type=video&pageIndex=1">视频</a>|
			<a name="other"
				href="Search.action?keyword=<s:property value='keyword'/>&type=other&pageIndex=1">其他</a>
			<div id="login">
				<a href="index.jsp">网站首页</a>|
				<a href="login.jsp">登录</a>
			</div>
		</div>
		<!--nav over -->
		<div id="Search_top">
			<br />
			<div>
				<img class="img1" src="images/logo.gif" />
			</div>
			<form action="Search.action" method="get">
				<div>
					<input type="text" class="keyword" name="keyword" id="keyword"
						autocomplete="off" value="<s:property value='keyword'/>" />
					<input type="submit" style="border: 0px" value=""
						onClick="return checkValid();" class="search_btn" id="search_btn" />
					<input type="hidden" name="type" value="all" />
				</div>
			</form>
		</div>
		<!--Search_top  over-->

		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>
	    
		<div id="file">
			<div id="flashContent" align="center">
			    <a id="viewerPlaceHolder" style="width:800px;height:900px;display:block"></a>			    	        	

	        <script type="text/javascript"> 
				var fp = new FlexPaperViewer(	
						 "http://<s:property value='ip' escape='false'/>:8080/MiracleSearch/FlexPaperViewer",
						 'viewerPlaceHolder', { config : {
						 SwfFile : escape("http://<s:property value='ip' escape='false'/>:8080/MiracleSearch/swfFile/<s:property value='fileName' escape='false'/>"),
						 Scale : 0.6, 
						 ZoomTransition : 'easeOut',
						 ZoomTime : 0.5,
						 ZoomInterval : 0.1,
						 FitPageOnLoad : true,
						 FitWidthOnLoad : true,
						 FullScreenAsMaxWindow : true,
						 ProgressiveLoading : true,
						 MinZoomSize : 0.2,
						 MaxZoomSize : 5,
						 SearchMatchAll : true,
						 InitViewMode : 'Portrait',
						 PrintPaperAsBitmap : true,
						 
						 ViewModeToolsVisible : true,
						 ZoomToolsVisible : true,
						 NavToolsVisible : true,
						 CursorToolsVisible : true,
						 SearchToolsVisible : true,
  						
  						 localeChain: 'zh_CN'
						 }});
	        </script>
        				
			</div>
			<!-- flashContent over -->
			<div id="relation_file">
				相关文档推荐<br>
				<s:if test="related.size>0">
					<s:iterator value="related" id="rel">
						<li>
							<s:property value="rel" escape="false" />
						</li>
					</s:iterator>
				</s:if>
				<s:else>
					<li>
						暂无相关搜索
					</li>
				</s:else>
			</div>
		</div>
		<!-- file over -->
		<br />

		<p class="CR_p">
			To view this page ensure that Adobe Flash Player version 8.0.0 or
			greater is installed.<br>
                    如果你看不到文档，请先到	
            <a target="_blank"
			    href="http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager04a.html#119065">
			Adobe's	website</a>.去获取Adobe Flash的安全认证<br>		
			&copy 2012 Miracle Workgroup|
			<a target="_blank" href="aboutUS.jsp">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</p>
	</body>
</html>
