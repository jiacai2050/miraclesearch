<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>图片详细信息</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link href="css/result.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
		<div style="height: 20px"></div>
		<div align="center" class="main" class="logo">
			<img src="images/logo.gif" height="100px" width="280px" />
		</div>
		<br />
		<br />
		<div align="center">
			<img align="middle" src='<s:property value="fileName"/>'>
			<br />
			<br />
			<br />
		<div class="CR_p">
			&copy 2012 Miracle Workgroup|
			<a target="_blank" href="aboutUS.jsp">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</div>
		</div>
	</body>
</html>
