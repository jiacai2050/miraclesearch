<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>MiracleSearch</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/index.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/index.js"></script>
	</head>

	<body>
		<div class="login" align="right">
			<a href="login.jsp">登录</a>
		</div>
		<div align="center" class="logo">
			<img src="images/logo.gif" height="100px" width="280px" />
		</div>
		<br>
		<div class="center">
			<div id="center">
				<form action="Search.action" method="get">
					<div id="search">
						<div id="nav">
							<div class="nav">
								<a href="#" name="all" class="selectedType">全部</a>
								<a name="document">文档</a>
								<a name="image">图片</a>
								<a name="music">音频</a>
								<a name="video">视频</a>
								<a name="other">其他</a>
							</div>
						</div>
						<div id="br"></div>
						<div class="search_div">
							<input type="text" class="keyword" name="keyword" id="keyword"
								autocomplete="off" onKeyDown="checkupdown(event);" />
							<a id="camera"> <img src="images/camera.png" /> </a>
							<input type="submit" style="border: 0px"
								onclick="return checkValid();" class="search_btn"
								id="search_btn" />
						</div>
						<div id="suggestion"></div>
					</div>
					<!--search over-->
					<input type="hidden" value="all" name="type">
					<input type="hidden" value="1" name="pageIndex">
				</form>
				<div id="picture_div">
					<p>
						识图搜索
						<br />
						<br />
						上传文件
					</p>
					<form enctype="multipart/form-data" method="post"
						action="Upload.action">
						<input type="file" id="pic" name="upload" />
						<input type="submit" value="" id="picture_btn" />
					</form>
					<a><img src="images/close.gif"
							onClick="hide_pic_div();" /> </a>
				</div>
			</div>
			<!--center id over-->
		</div>
		<!--center class over-->
		<p class="info" align="center">
			<a href=#
				onClick="this.style.behavior='url(#default#homepage)';this.setHomePage('http://localhost:8080/MiracleSearch');">设为主页</a>
			<a href="aboutUS.jsp" target="_blank">关于我们</a>
			<a href="instruction.jsp" target="_blank">使用说明</a>
			<br>
			&copy 2012 Miracle Workgroup
		</p>
	</body>
</html>