<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>索引创建成功</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/404.css" rel="stylesheet" type="text/css" />
		<!--
	    <link rel="stylesheet" type="text/css" href="styles.css">
	    -->
	</head>

	<body>
		<div class="center">
			<div id="center">
				<div id="title">
					页面提醒
				</div>
				<br />
				<br />
				<br />
				<div id="content">
					<br />
					<p class="success">
						索引创建成功
					</p>
					<br>
					<br>
					<center>
						<p class="click">
							下面可以进行搜索了
							<a href="index.jsp">首页</a>
						</p>
						<br>
					</center>
				</div>
			</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div class="CR_p">
			&copy 2012 Miracle Workgroup|
			<a target="_blank" href="aboutUS.jsp">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</div>
	</body>
</html>