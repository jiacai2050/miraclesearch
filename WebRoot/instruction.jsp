<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>使用说明</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/index.css" rel="stylesheet" type="text/css" />
		<!--
	    <link rel="stylesheet" type="text/css" href="styles.css">
	    -->
	</head>

	<body>
		<br />
		<table width="956" height="500" border="0" cellpadding="0"
			cellspacing="0" align="center">
			<tr>
				<td align="center">
					<img src="images/logo.gif" alt="使用帮助" width="200px" height="80px" />
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td>
					<h2>
						1.搜索入门
					</h2>
					MiracleSearch简单方便。您只需要在搜索框内输入需要查询的内容，敲回车键，或者鼠标点击搜索框右侧的闪应搜索按钮，就可以得到最符合查询需求的网页内容。MiracleSearch，就是这么简单！
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td>
					<h2>
						2.使用多个词语搜索
					</h2>
					输入多个词语搜索（不同字词之间用一个空格隔开），可以获得更精确的搜索结果。
					<br />
					例如：想了解上海人民公园的相关信息，在搜索框中输入 [初二 语文] 获得的搜索效果会比输入 [语文] 得到的结果更好。
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td>
					<h2>
						3.使用拼音词语搜索
					</h2>
					例如：在搜索框中输入[kejian] [kj]和输入[课件]的结果相同。
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td>
					<h2>
						4.布尔搜索
					</h2>
					例如：在搜索框中输入[课件 - 奥运]，搜索结果中就不会出现有关奥运的课件。
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td>
					<h2>
						5.图片高级搜索
					</h2>
					在图片高级搜索中，你可以上传一张图片，引擎会根据你上传的图片来进行结果的匹配 。
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td height="52">
					<h2>
						6.不能正常访问MiracleSearch
					</h2>
					请先确认服务器是否能够正常访问。确定服务器无故障后，请使用
					<span lang="EN-US" xml:lang="EN-US">IP</span>地址
					<span lang="EN-US" xml:lang="EN-US"><a
						href="http://localhost:8080/MiracleSearch/" target="_blank">http://localhost:8080/MiracleSearch</a>
					</span>访问，如果可以访问则请在您的电脑中
					<a
						href="http://www.baidu.com/s?wd=c%C5%CC%B2%E9%D5%D2hosts&amp;cl=3"
						target="_blank">查找hosts文件</a>，用文本编辑器打开，查看是否有 MiracleSearch
					的记录项，如有请删除该记录，并重新启动浏览器。
					<br />
				</td>
			</tr>
			<tr style="font-size: 17px">
				<td height="64">
					<strong><h2>
							7.以上方法不能解决问题?
						</h2>
					</strong> 若以上方法不能帮您解决问题，请提供以下信息，并将执行结果发送至liujiacai@163.com，以便我们能更好的为您分析问题。
					<br />
					1.请您详细描述访问闪应产品时出现的异常并提供异常页面的截图，以及描述一下您的网络环境（如您所在的地区、上网方式、公网IP地址、代理IP地址、以及您周围人群访问相同页面时的状况等）
					<br />
					2.请您点击“开始”-“运行”-输入cmd-确定，分别执行ipconfig/all命令和ping
					localhost:8080命令以及nslookup localhost:8080命令和tracert
					localhost:8080命令，并在地址栏中输入http://localhost:8080
					，将以上5个操作的执行结果保存（截图、文本均可）发送给我们。
					<br />
				</td>
			</tr>
		</table>
		<br />
	</body>
</html>
