
function show_res_div(obj) {
	$("#resources_div").css("display", "block");
	$("#word_div").css("display", "none");
	$("#stop_div").css("display", "none");
}
function show_word_div(obj) {
	$("#resources_div").css("display", "none");
	$("#word_div").css("display", "block");
	$("#stop_div").css("display", "none");
	$.ajax({url:"dic.action", type:"POST", dataType:"json", data:{"file":"ext.dic"}, success:function (returnedData) {
		var html = "";
		for (var i = 0; i < returnedData.words.length; i++) {
			html += (returnedData.words[i] + "&nbsp;");
		}
		$("#noun_content").html(html);
	}});//ajax
}
function show_stop_div(obj) {
	$("#resources_div").css("display", "none");
	$("#word_div").css("display", "none");
	$("#stop_div").css("display", "block");
	$.ajax({url:"dic.action", type:"POST", dataType:"json", data:{"file":"mystop.dic"}, success:function (returnedData) {
		var html = "";
		for (var i = 0; i < returnedData.words.length; i++) {
			html += (returnedData.words[i] + "&nbsp;");
		}
		$("#stop_content").html(html);
	}});//ajax
}
function expand(el) {
	childObj = document.getElementById("child" + el);
	if (childObj.style.display == "none") {
		childObj.style.display = "block";
	} else {
		childObj.style.display = "none";
	}
	return;
}
function submitDic(obj) {
	var file = "ext.dic";
	var div = "#noun_content";
	if ($(obj).attr("name") == "myStop") {
		file = "myStop.dic";
		div = "#stop_content";
	}
	var newDic = $(obj).prev().val();
	$.ajax({url:"dic.action", type:"POST", dataType:"json", data:{"newDic":newDic, "file":file}, success:function (returnedData) {
		var old = $(div).html();
		$(div).html(old + "<br><br><font color=red>" + newDic + "</font>\u66f4\u65b0\u6210\u529f\u4e86,\u65b0\u7684\u8bcd\u5e93\u5c06\u5728\u7cfb\u7edf\u91cd\u65b0\u542f\u52a8\u540e\u751f\u6548<br>");
		$(obj).prev().val("");
	}});//ajax
}

