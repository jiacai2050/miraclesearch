// JavaScript Document
$(function () {  // 页面DOM加载完毕后执行
	init();
}); // jquery ready over
function init() {
	$(".nav a[name!='image']").click(function () {
		navigate(this);
		$("#camera").css("display", "none");
	});
	$(".nav a[name='image']").click(function () {
		navigate(this);
		$("#camera").css("display", "block");
	});
	$("#pic").change(function () {
		$("#picture_btn").css("display", "block");
	});
	$("#keyword").focus();
	$("#camera").click(function () {
		show_pic_div();
	});
	$("#search_btn").val("");
	$("#suggestion").addClass("suggestion_hidden");
	$("body").click(function () {
		$("#suggestion").removeClass("suggestion_show");
		$("#suggestion").addClass("suggestion_hidden");
	});
	googleSuggestion();
}
function navigate(obj) {
	$(".nav a").each(function () {
		$(this).removeClass("selectedType");
	});
	$(obj).addClass("selectedType");
	$("input[name=type]").val(obj.name);//改变隐藏域的type
}
function googleSuggestion() {
	if ($.browser.msie) {
		$("#keyword")[0].attachEvent("onpropertychange", myajax);
	} else {
		$("#keyword").bind("input", myajax);
	}
}//googleSuggestion() end
function myajax() {
	var keyword = $("#keyword").val();
	keyword = $.trim(keyword);
	if (keyword != "") {
		$.ajax({url:"GoogleSuggestion.action", type:"POST", dataType:"json", data:{"keyword":keyword}, success:function (returnedData) {
					//alert(returnedData.list[0]);
			var html = "<table width=484px border=0>";
			for (var i = 0; i < returnedData.list.length; i++) {
				html += ("<tr onmouseover=mouseoverhandler(this); onmouseout=mouseouthandler(this); onclick=clickhandler(this);><td>" + returnedData.list[i] + "</td></tr>");
			}
			html += "</table>";
			$("#suggestion").html(html);
			$("#suggestion").removeClass("suggestion_hidden");
			$("#suggestion").addClass("suggestion_show");
			$("#suggestion").css("height", 22 * (returnedData.list.length));
		}});//ajax
	}//if 
		else {
		$("#suggestion").removeClass("suggestion_show");
		$("#suggestion").addClass("suggestion_hidden");
			
		} 
}// myajax
function checkValid() {

	setKeyword();
	var keyword = $("#keyword").val();
	keyword = $.trim(keyword);
	if (keyword.length < 1) {
		alert("\u5173\u952e\u5b57\u4e0d\u80fd\u4e3a\u7a7a");
		return false;
	} else {
		return true;
	}
}  //checkValid()
function setKeyword(){
	var sel = $("#suggestion tr[class='selectedItem']");
	if(sel.length > 0) {
		$("#keyword").val(sel.text());
	}
}
function checkupdown(event) {
	if(event.keyCode == 13){//回车键
		setKeyword();
	}
	//alert(event.keyCode);
	if ($("#suggestion").html() != "" && event.keyCode == 40) {  //down
		if ($("#suggestion tr[class='selectedItem']").length == 0) {
			$("#suggestion tr:first").addClass("selectedItem");
		//	$("#keyword").val($("#suggestion tr:first").text());
		} else {
			var current = $("#suggestion tr[class='selectedItem']");
			current.next().addClass("selectedItem");
		//	$("#keyword").val(current.next().text());
			current.removeClass();
		}
	}
	if ($("#suggestion").html() != "" && event.keyCode == 38) {  //up key
		if ($("#suggestion tr[class='selectedItem']").length == 0) {
			$("#suggestion tr:last").addClass("selectedItem");
		//	$("#keyword").val($("#suggestion tr:first").text());
		} else {
			var current = $("#suggestion tr[class='selectedItem']");
			current.prev().addClass("selectedItem");
		//	$("#keyword").val(current.prev().text());
			current.removeClass();
		}
	}
}//checkupdown
function mouseoverhandler(obj) {

	$("#suggestion tr").each(function () {
		$(this).removeClass("selectedItem");
	});
	//$("#keyword").val($(obj).text());
	$(obj).addClass("selectedItem");
}
function mouseouthandler(obj) {

	$(obj).removeClass("selectedItem");
}
function clickhandler(obj) {
	$("#keyword").val($(obj).text());
	document.getElementById("search_btn").click();
}


//2012/8/4 新增by刘雨
function show_pic_div(obj) {
	$("#picture_div").css("display", "block");
}
function hide_pic_div() {
	$("#picture_div").css("display", "none");
}

