<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>管理员登陆</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link href="css/login-admin.css" rel="stylesheet" type="text/css" />
	</head>

	<body class="login-body">
       <form name="adminlogin" action="Login.action" method="post">
          <table cellSpacing="0" cellPadding="0" width="1004px" align="center">
            <Tbody>
               <tr>
                  <td colSpan=6>
                      <img height="92px" width="345px" alt="" 
                       src="images/login/crm_1.gif"></td>
                  <td colSpan=4>
                      <img height="92px" width="452px" alt="" 
                      src="images/login/crm_2.gif" ></td>
                  <td>
                      <img height="92px" width="207px" alt="" 
                      src="images/login/crm_3.gif"></td>
               </tr>
               <tr>
                  <td colSpan=6>
                      <img height="98px" width="345px" alt="" 
                      src="images/login/crm_4.gif" ></td>
                  <td colSpan=4>
                      <img height="98" width="452px" alt="" 
                      src="images/login/crm_5.gif" ></td>
                  <td><img height="98px" width="207px" alt="" 
                       src="images/login/crm_6.gif"></td>
               </tr>
               <tr>
                  <td rowSpan=5>
                      <img height="370px" width="59px" alt="" 
                      src="images/login/crm_7.gif"></td>
                  <td colSpan=5>
                      <img height="80px" width="286px" alt="" 
                      src="images/login/crm_8.gif"></td>
                  <td colSpan=4>
                      <img height="80px" width="452px" alt="" 
                      src="images/login/crm_9.gif" ></td>
                  <td><img height="80px" width="207px" alt="" 
                      src="images/login/crm_10.gif"></td>
               </tr>
               <tr>
                  <td><img height="110px" width="127px" alt="" 
                      src="images/login/crm_11.gif" ></td>
                  <td background=images/login/crm_12.gif colSpan=6>
                      <table cellSpacing="0" cellPadding="0" width="98%">
                         <Tbody>
                            <tr>
                               <td>
                                 <table cellSpacing="1" cellPadding="0" width="100%">
                                    <Tbody>
                                        <tr>
                                           <td align="center" width="81px"><font color=#ffffff>用户名：</font></td>
                                           <td><input class="regtxt" type="text" title=请填写用户名 maxLength=16 size=16 name="username" value=""></td>
                                        </tr>
                                        <tr>
                                           <td align="center" width="81px"><font color=#ffffff>密&nbsp;码：</font></td>
                                           <td><input class="regtxt" title=请填写密码 type="password" maxLength=16 size=16 name="password" value=""></td>
                                        </tr>           
                                    </Tbody>
                                 </table>
                               </td>
                            </tr>
                         </Tbody>
                     </table>
                 </td>
                 <td colSpan=2 rowSpan=2>
                     <img height="158px" width="295px" alt="" src="images/login/crm_13.gif"></td>
                 <td rowSpan=2><img height="158px" width="207px" alt="" src="images/login/crm_14.gif"></td>
             </tr>
             <tr>
                <td rowSpan=3>
                    <img height="180px" width="127px" alt="" src="images/login/crm_15.gif"></td>
                <td rowSpan=3>
                    <img height="180px" width="24px" alt="" src="images/login/crm_16.gif"></td>
                <td><a href="admin.jsp"><input title=登录后台 type=image height="48" width="86px" alt="" src="images/login/crm_17.gif" name=image></a></td>
                <td><img height="48px" width="21px" alt="" src="images/login/crm_18.gif"></td>
                <td colSpan=2><a href="index.jsp"><img title=返回首页 height="48px" width="84px" border="0" alt="" src="images/login/crm_19.gif"></a></td>
                <td><img height="48px" width="101px" alt="" src="images/login/crm_20.gif"></td>
             </tr>
             <tr>
                <td colSpan=5 rowSpan=2><img height="132px" width="292px" alt="" src="images/login/crm_21.gif"></td>
                <td rowSpan=2>
                    <img height="132px" width="170px" alt="" src="images/login/crm_22.gif"></td>
                <td colSpan=2>
                    <img height="75px" width="332px" alt="" src="images/login/crm_23.gif"></td>
             </tr>
             <tr>
                <td colSpan=2>
                    <img height="57px" width="332px" alt="" src="images/login/crm_24.gif"></td>
            </tr>
            <tr>
               <td><img height="1px" width="59px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="127px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="24px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="86px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="21px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="28px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="56px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="101px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width=170px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="125px" alt="" src="images/login/spacer.gif"></td>
               <td><img height="1px" width="207px" alt="" src="images/login/spacer.gif"></td>
           </tr>
        </Tbody>
     </table>
   </form>
</body>
</html>