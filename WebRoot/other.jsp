<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>压缩文件</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/404.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div style="height: 20px"></div>
		<div align="center" class="main" class="logo">
			<img src="images/logo.gif" height="80px" width="250px" />
		</div>
		<br />
		<center>
			<s:property value="file" />
			里面有：
			<s:iterator value="result" id="res">
				<table>
					<tr>
						<td>
							<s:property value="res" />
							<a
								href="http://<s:property value='ip'/>:8080/MiracleSearch/Download.action?kind=other&fileName=<s:property value='res'/>">下载</a>
						</td>
					</tr>
				</table>
			</s:iterator>
		</center>
	</body>
</html>