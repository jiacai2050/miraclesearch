<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>搜索结果</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/result.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript">
		$(function() {
			
			$($("#page a")[(<s:property value="pageIndex"/> - 1)]).removeClass("pages");	
			$($("#page a")[(<s:property value="pageIndex"/> - 1)]).addClass("current");
			
			$("#nav a").each(function() {
				$(this).addClass("navigations");
			});
			
			$("#nav a[name=<s:property value='type'/>]").removeClass("navigations");
			$("#nav a[name=<s:property value='type'/>]").addClass("current");	
		});		
		</script>
	</head>

	<body>
		<div id="nav">
			<a name="all"
				href="Search.action?keyword=<s:property value='keyword'/>&type=all&pageIndex=1">全部</a>|
			<a name="document"
				href="Search.action?keyword=<s:property value='keyword'/>&type=document&pageIndex=1">文档</a>|
			<a name="image"
				href="Search.action?keyword=<s:property value='keyword'/>&type=image&pageIndex=1">图片</a>|
			<a name="music"
				href="Search.action?keyword=<s:property value='keyword'/>&type=music&pageIndex=1">音频</a>|
			<a name="video"
				href="Search.action?keyword=<s:property value='keyword'/>&type=video&pageIndex=1">视频</a>|
			<a name="other"
				href="Search.action?keyword=<s:property value='keyword'/>&type=other&pageIndex=1">其他</a>
			<div id="login">
				<a href="index.jsp">网站首页</a>|
				<a href="login.jsp">登录</a>
			</div>
		</div>
		<!--nav over -->

		<div id="Search_top">
			<br />
			<div>
				<img class="img1" src="images/logo.gif" />
			</div>
			<form action="Search.action" method="get">
				<div>
					<input type="text" class="keyword" name="keyword" id="keyword"
						autocomplete="off" value="<s:property value='keyword'/>" />
					<input type="submit" style="border: 0px" value=""
						onClick="return checkValid();" class="search_btn" id="search_btn" />
					<input type="hidden" name="type" value="<s:property value='type'/>" />
				</div>
			</form>
			<div class="search_time">
				搜索到
				<s:property value="resultBean.length" />
				项结果，用时
				<s:property value="resultBean.time" />
				ms
			</div>
		</div>
		<!--Search_top  over-->
		<s:if test="hanzi.size!=0">
		您要找的是不是：
		<s:iterator value="hanzi" id="hz">
				<s:property value="hz" escape="false"></s:property> &nbsp;
		</s:iterator>
		</s:if>
		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>

		<%
			String exec = (String) request.getAttribute("calc_expression");
			String result = (String) request.getAttribute("calc_result");
			if (exec != null) {
				out.print(exec + "=" + result);
				out.print("&nbsp;" + "<a target='_blank' href='calc.html'>展开智能计算器</a>");
			}
			String expand = (String) request.getAttribute("expand");
			if(expand != "") {
				out.print(expand + "<br>");		
			}
			
		%>
		<div id="mainBody" class="content">
			<table width="606px">
				<s:iterator value="resultBean.resultList" status="st" id="re">
					<tr>
						<td>
							<font class="title"><s:property value='title'
									escape="false"></s:property> </font>
							<img
								src='images/icon/<s:property value="kind" escape="false"/>.jpg' />
							<s:property value="kind" escape="false"></s:property>

						</td>
					</tr>
					<tr>
						<td class="description">
							<s:property value="content" escape="false"></s:property>
							...
						</td>
					</tr>
					<tr>
						<td class="kind">
							作者：
							<s:property value="author" escape="false"></s:property>
							&nbsp;&nbsp; 出版社：
							<s:property value="publisher" escape="false"></s:property>
							&nbsp;
							<font class="date"><s:property value="date" escape="false"></s:property>
							</font>
						</td>
					</tr>
					<tr>
						<td class="url">
							URL：
							<s:property value="url" escape="false"></s:property>
							<a
								href="<s:property value="download" escape="false"></s:property>">下载</a>
						</td>
					</tr>
					<tr height=25>
						<td>

						</td>
					</tr>
				</s:iterator>
			</table>
		</div>
		<s:if test="resultBean.resultList.size<2">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
		</s:if>
		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>

		<div id="page">
			<s:iterator value="resultBean.pageList" id="pageNO">
				<s:property value="pageNO" escape="false" />
			</s:iterator>
		</div>
		<br />

		<form action="Search.action" method="get">
			<div id="Search_bottom">
				<input type="text" class="keyword_bottom" name="keyword"
					id="keyword" autocomplete="off"
					value="<s:property value='keyword'/>" />
				<input type="submit" style="border: 0px" value=""
					onClick="return checkValid();" class="search_btn_bottom"
					id="search_btn" />
				<input type="hidden" name="type" value="<s:property value='type'/>" />
			</div>
		</form>
		<br />
		<br />
		<br />
		<div class="CR_p">
			&copy 2012 Miracle Workgroup|
			<a target="_blank" href="aboutUS.jsp">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</div>
		<div id="related_search">
			<p class="rs1">
				相关搜索：
			</p>
			<p class="rs2">
				<s:if test="related.size>0">
					<s:iterator value="related" id="rel">
						<li>
							<s:property value="rel" escape="false" />
						</li>
					</s:iterator>
				</s:if>
				<s:else>
					暂无相关搜索
				</s:else>
			</p>
		</div>
		<!--RelatedSearch over-->
	</body>
</html>
