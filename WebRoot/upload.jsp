<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
	<head>
		<title>资源上传</title>
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/404.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div style="height: 20px"></div>
		<div align="center" class="main" class="logo">
			<img src="images/logo.gif" height="80px" width="250px" />
		</div>
		<s:form action="UploadRes" method="POST" enctype="multipart/form-data">
			<table align="center" cellSpacing="0" cellPadding="0" width="360px"
				height="220px">
				<tr height="10px">
					<td>
						<s:file name="upload" label="请选择"></s:file>
					</td>
				</tr>
				<tr height="10px">
					<td>
						<s:textfield name="keywords" label="关键字" />
					</td>
				</tr>
				<tr height="10px">
					<td>
						<s:textfield name="describe" label="描述" />
					</td>
				</tr>
				<tr height="10px">
					<td>
						<s:textfield name="author" label="作者" />
					</td>
				</tr>
				<tr height="10px">
					<td>
						<s:textfield name="publisher" label="发布者" />
					</td>
				</tr>
				<tr>
					<td>
						<s:submit value="上传" />
					</td>
				</tr>
			</table>
		</s:form>
	</body>
</html>
