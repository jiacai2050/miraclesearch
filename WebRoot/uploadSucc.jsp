<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>上传成功</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="css/404.css" rel="stylesheet" type="text/css" />
		<!--
	    <link rel="stylesheet" type="text/css" href="styles.css">
	    -->
	</head>

	<body>
		<div class="center">
			<div id="center">
				<div id="title">
					页面提醒
				</div>
				<br />
				<br />
				<br />
				<div id="content">
					<br />
					<p class="success">
						<s:property value="fileName" />
						上传成功
					</p>
					<br>
					<br>
					<center>
						<p class="click">
							点击这里
							<a href="Reindex">重建索引</a>
						</p>
						<br>
					</center>
				</div>
			</div>
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div class="CR_p">
			&copy 2012 Miracle Workgroup|
			<a href="aboutUS.jsp" target="_blank">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</div>
	</body>
</html>