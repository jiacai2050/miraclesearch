<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="images/favicon.ico" rel="icon" type="image/x-icon" />
		<script type="text/javascript" src="js/flowplayer-3.2.11.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/video.css">
		<link rel="stylesheet" type="text/css" href="css/result.css">
		<title>视频预览</title>
	</head>

	<body>
		<div id="nav">
			<a name="all"
				href="Search.action?keyword=<s:property value='keyword'/>&type=all&pageIndex=1">全部</a>|
			<a name="document"
				href="Search.action?keyword=<s:property value='keyword'/>&type=document&pageIndex=1">文档</a>|
			<a name="image"
				href="Search.action?keyword=<s:property value='keyword'/>&type=image&pageIndex=1">图片</a>|
			<a name="music"
				href="Search.action?keyword=<s:property value='keyword'/>&type=music&pageIndex=1">音频</a>|
			<a name="video"
				href="Search.action?keyword=<s:property value='keyword'/>&type=video&pageIndex=1">视频</a>|
			<a name="other"
				href="Search.action?keyword=<s:property value='keyword'/>&type=other&pageIndex=1">其他</a>
			<div id="login">
				<a href="index.jsp">网站首页</a>|
				<a href="login.jsp">登录</a>
			</div>
		</div>
		<!--nav over -->
		<div id="Search_top">
			<br />
			<div>
				<img class="img1" src="images/logo.gif" />
			</div>
			<form action="Search.action" method="get">
				<div>
					<input type="text" class="keyword" name="keyword" id="keyword"
						autocomplete="off" value="<s:property value='keyword'/>" />
					<input type="submit" style="border: 0px" value=""
						onClick="return checkValid();" class="search_btn" id="search_btn" />
					<input type="hidden" name="type" value="all" />
				</div>
			</form>

		</div>
		<!--Search_top  over-->
		<hr style="border: 3px double #987cb9" width="100%" color=#987cb9>

		<div class="center">
			<div id="page">
				<h1>
					<s:property value="fileName" />
				</h1>
				<br />
				<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->

				<div align="center">
					<a
						href="http://pseudo01.hddn.com/vod/demo.flowplayervod/flowplayer-700.flv"
						style="display: block; width: 520px; height: 330px" id="player">
					</a>
				</div>
				<script>
                var player = flowplayer("player", "flowplayer-3.2.12.swf", {
	                clip : {
		                provider : 'rtmp',
		                live : false,
		                autoBuffering : true, //是否自动缓冲视频，默认true
		                autoPlay : true,
		                url : '<s:property value="fileName"/>' //此处为播放文件名，可由网站参数指定，视频文件位于前文提到的Red5根目录下webapps/oflaDemo/streams
	                },

	                plugins : {
		                rtmp : {
			                url : 'flowplayer.rtmp-3.2.10.swf',
			                netConnectionUrl : 'rtmp://<s:property value="ip"/>/oflaDemo/'
		                },
		                controls : {
		              	    url : 'flowplayer.controls-3.2.12.swf',
			                autoHide : 'always',
			                play : true,
			                scrubber : true,
			                playlist : false,
			                tooltips : {
			             	    buttons : true,
			            	    play : '播放',
			            	    fullscreen : '全屏',
			            	    fullscreenExit : '退出全屏',
		             		    pause : '暂停',
		             		    mute : '静音',
			            	    unmute : '取消静音'
		               	    }
		                }
	                }
                //end for plugins
		        });
                </script>
			</div>
		</div>
		<div class="CR_p">
			&copy 2012 Miracle Workgroup|
			<a target="_blank" href="aboutUS.jsp">关于我们</a>|
			<a href="instruction.jsp" target="_blank">使用说明</a>
		</div>
	</body>
</html>