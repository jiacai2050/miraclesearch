package com.miracle.action;

import java.util.List;

import com.miracle.bean.ImageSearchBean;
import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class AdvancedImageSearch extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileName;
	private List<String> result;
	private ImageSearchBean isb;
	private int pageIndex = 1;

	@Override
	public String execute() throws Exception {
		doImageSearch();
		
		return SUCCESS;
	}

	private void doImageSearch() {

		isb = new ImageSearchBean();
		result = isb.doImageSearch(Constants.MIRACLESEARCH + "upload/"
				+ fileName, pageIndex, fileName);
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public ImageSearchBean getIsb() {
		return isb;
	}

	public void setIsb(ImageSearchBean isb) {
		this.isb = isb;
	}

}
