package com.miracle.action;

import java.util.ArrayList;
import java.util.List;

import com.miracle.bean.FileDecompressionBean;
import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class Decompression extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ip;
	private String file;
	private List<String> result;
	@Override
	public String execute() throws Exception {

		FileDecompressionBean fdb = new FileDecompressionBean(ip,file);
		List<String> list = fdb.work();
		result = new ArrayList<String>();
		if (list != null) {
			for (String str : list) {
				str = str.replace(Constants.MIRACLESEARCH, "");
//				str = str.replace("\\", "/");
				//result.add(str.substring(str.lastIndexOf(File.separatorChar)+1));
				result.add(str);
//				System.out.println(str);
			}
			return SUCCESS;	
		} else {
			return NONE;
		}
		
	}
	public List<String> getResult() {
		return result;
	}
	public void setResult(List<String> result) {
		this.result = result;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}

}
