package com.miracle.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class DicManager extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<String> words;

	private String newDic;

	private String file;

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getNewDic() {
		return newDic;
	}

	public void setNewDic(String newDic) {
		this.newDic = newDic;
	}

	public List<String> getWords() {
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

	@Override
	public String execute() throws Exception {
		File file = new File(Constants.MIRACLESEARCH + "WEB-INF/classes/"
				+ getFile());
		if (newDic != null) {
			FileOutputStream fos = new FileOutputStream(file, true);
			OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
			osw.append("\r\n" + newDic);
			osw.flush();
			osw.close();
			System.out.println("更新成功了");
		} else {
			words = new ArrayList<String>();
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String sLine = "";
			while ((sLine = br.readLine()) != null) {
				// sbContent = sbContent.append(sLine.replace("\n",
				// "").replace("\r",
				// "").trim());
				words.add(sLine);
			}

		}

		return SUCCESS;
	}

}
