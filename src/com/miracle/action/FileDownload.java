package com.miracle.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class FileDownload extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fileName;

	private String kind;

	public InputStream getTargetFile() {
		File resources = null;
		try {
			if ("other".equalsIgnoreCase(kind)) {
				resources = new File(Constants.MIRACLESEARCH + fileName);
			} else {
				resources = new File(Constants.WEBAPP + "resources/" + fileName);
			}
//			System.out.println(resources.getAbsolutePath());
			HttpServletRequest request = ServletActionContext.getRequest();
			String agent = request.getHeader("User-Agent").toLowerCase();
			if (null != agent) {
				if (agent.indexOf("msie") != -1) { // 如果客户端浏览器为IE
					fileName = java.net.URLEncoder.encode(fileName, "UTF-8");
				} else {
					fileName = new String(fileName.getBytes("UTF-8"),
							"ISO-8859-1");
				}
			}
			return new FileInputStream(resources);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String execute() throws Exception {
//		System.out.println("kind = " + kind);
		if (new File(Constants.WEBAPP + "resources/" + fileName).exists()
				|| new File(Constants.MIRACLESEARCH + fileName).exists()) {
			return SUCCESS;
		}
		return NONE;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}
	
}
