package com.miracle.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;

import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class GoogleSuggestion extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient String keyword;//不会被序列化
	private List<String>list;

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String execute() throws Exception {
		list = new ArrayList<String>();
		//String path = ServletActionContext.getServletContext().getRealPath("/");
		if (null != getKeyword()) {
			doGoogleSuggestion(Constants.MIRACLESEARCH);	
		} else {
			list.add("");
		}
			
		return SUCCESS;
	}

	private StringBuffer doGoogleSuggestion(String path) {
		try {
			File indexPath = new File(path, "index");
			FSDirectory fsd = FSDirectory.open(indexPath);
			IndexReader ir = IndexReader.open(fsd);
			IndexSearcher is = new IndexSearcher(ir);
			// IKAnalyzer analyzer = new IKAnalyzer();
			//System.out.println(keyword);
			Term prefix = new Term("title", keyword);
			PrefixQuery query = new PrefixQuery(prefix);

			ScoreDoc[] hits = is.search(query, 6).scoreDocs;
			for (int i = 0; i < hits.length; i++) {
				Document hitDoc = is.doc(hits[i].doc);
				list.add(hitDoc.get("title"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
