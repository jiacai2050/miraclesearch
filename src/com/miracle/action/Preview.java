package com.miracle.action;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.miracle.bean.FileTransformBean;
import com.miracle.util.Constants;
import com.miracle.util.RelatedSearch;
import com.opensymphony.xwork2.ActionSupport;

public class Preview extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fileName;

	private String id;

	private String ip;

	private List<String> related;

	public List<String> getRelated() {
		return related;
	}

	public void setRelated(List<String> related) {
		this.related = related;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String execute() throws Exception {
//		if (!fileName.equals(new String(fileName.getBytes("UTF-8"), "UTF-8"))) {
//			// 解决IE8中URL传参乱码 通过在页面中添加@java.net.URLEncoder@encode解决
//			fileName = new String(fileName.getBytes("ISO-8859-1"), "UTF-8");
//
//		}
		String regEx = "\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(fileName);
		if (m.find()) {
			setIp(m.group());
		}

		if (fileName.endsWith("doc") || fileName.endsWith("ppt")
				|| fileName.endsWith("txt") || fileName.endsWith("pdf")
				|| fileName.endsWith("docx") || fileName.endsWith("pptx")
				|| fileName.endsWith("xls")) {

			String tmp = fileName.substring(fileName.lastIndexOf("/") + 1);
			String pdf = "";
			String swf = "";
			if (tmp.endsWith("pdf")) {
				pdf = Constants.WEBAPP + "resources/" + tmp;
				swf = Constants.MIRACLESEARCH + "swfFile/" + id + ".swf";
			} else {
				pdf = Constants.MIRACLESEARCH + "pdfFile/" + id + ".pdf";
				swf = Constants.MIRACLESEARCH + "swfFile/" + id + ".swf";

			}

			String source = Constants.WEBAPP + "resources/" + tmp;

			String keyword = tmp.substring(0, tmp.lastIndexOf("."));

			setRelated(new RelatedSearch().doRelateSearch(keyword));

			setFileName(id + ".swf");

			FileTransformBean ftb = new FileTransformBean(ip);
			if (ftb.docTrans(source, pdf, swf)) {
				return "document";
			} else {
				return NONE;
			}
		} else if (fileName.endsWith("jpg") || fileName.endsWith("bmp")
				|| fileName.endsWith("png") || fileName.endsWith("gif")) {

			return "image";

		} else if (fileName.endsWith("avi") || fileName.endsWith("rm")
				|| fileName.endsWith("rmvb")) {

			String tmp = fileName.substring(fileName.lastIndexOf("/") + 1);
			setFileName(tmp.replaceAll("\\.\\w{1,5}$", ".flv"));

			FileTransformBean ftb = new FileTransformBean(ip);
			if (ftb.videoTrans(Constants.WEBAPP + "resources/" + tmp, System
					.getenv("red5_home")
					+ "/webapps/oflaDemo/streams/" + getFileName())) {
				return "video";

			} else {
				return NONE;
			}
		} else if (fileName.endsWith("mp3") || fileName.endsWith("wma")) {

			String tmp = fileName.substring(fileName.lastIndexOf("/") + 1);
			setFileName(tmp.replaceAll("\\.\\w{1,5}$", ".mp3"));

			FileTransformBean ftb = new FileTransformBean(ip);
			if (ftb.musicTrans(Constants.WEBAPP + "resources/" + tmp, System
					.getenv("red5_home")
					+ "/webapps/oflaDemo/streams/" + getFileName())) {

				return "music";

			} else {
				return NONE;
			}

		} else {

			setFileName(fileName.substring(fileName.lastIndexOf("/") + 1));

			return "other";

		}

	}
}
