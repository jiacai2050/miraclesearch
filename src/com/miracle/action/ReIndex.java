package com.miracle.action;

import java.io.File;

import com.miracle.index.ImageIndexMaker;
import com.miracle.index.IndexMaker;
import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class ReIndex extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String execute() throws Exception {
		File file = new File(Constants.MIRACLESEARCH, "index");
		if (IndexMaker.doCreate(file) && new ImageIndexMaker().doIndex()) {
			return SUCCESS;
		} else {
			return NONE;
		}

	}
}
