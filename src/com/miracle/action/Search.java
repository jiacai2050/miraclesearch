package com.miracle.action;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.struts2.ServletActionContext;

import com.miracle.bean.ResultBean;
import com.miracle.calc.CalcParser;
import com.miracle.calc.ParseException;
import com.miracle.util.Constants;
import com.miracle.util.RelatedSearch;
import com.opensymphony.xwork2.ActionSupport;

public class Search extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private ResultBean resultBean;

	private String keyword;

	private int pageIndex = 0;

	private String type;

	private List<String> related;

	private Set<String> hanzi;

	public List<String> getRelated() {
		return related;
	}

	public void setRelated(List<String> related) {
		this.related = related;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ResultBean getResultBean() {
		return resultBean;
	}

	public void setResultBean(ResultBean resultBean) {
		this.resultBean = resultBean;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String execute() throws Exception {

		// keyword.replaceAll("<font color=\"red\">", "");
		// keyword.replaceAll("</font>", "");
		// calc(keyword);

		RelatedSearch relatedSearch = new RelatedSearch();
		Pattern pattern = Pattern.compile("[\\u4e00-\\u9fa5]+");
		Matcher matcher = pattern.matcher(keyword);
		if (!matcher.find()) { // 如果关键字不包含汉字，就进行汉字提示功能
			setHanzi(relatedSearch.hanziSuggestion(keyword, type));

			pattern = Pattern.compile("[1-9]+");
			matcher = pattern.matcher(keyword);
			if (matcher.find()) { // 如果关键字包含数字，就进行Miracle计算
				calc(keyword);

			}
		}

		expandFunc();

		setRelated(relatedSearch.doRelateSearch(keyword));
		resultBean = new ResultBean();
		if (pageIndex == 0) {
			pageIndex = 1;
		}
		boolean hasResult = resultBean.doRMISearch(Constants.MIRACLESEARCH,
				keyword, pageIndex, type);
		if (hasResult) {
			if ("image".equals(type)) {
				return "image";
			} else {
				return SUCCESS;
			}

		} else {
			return NONE;
		}

	}

	private void calc(String exec) {

		try {
			CalcParser parser = new CalcParser(exec);
			double d = parser.elexpr();

			ServletActionContext.getRequest().setAttribute("calc_result",
					String.valueOf(d));
			ServletActionContext.getRequest().setAttribute("calc_expression",
					exec);
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("calc_result",
					"<font color=red>您输入的表达式不是合法的数学表达式</font>");
			ServletActionContext.getRequest().setAttribute("calc_expression",
					exec);
		}
	}

	private void expandFunc() {
		String tool = "";
		if ("天气".equals(keyword)) {
			tool = "<iframe src='http://www.thinkpage.cn/weather/weather.aspx?uid=&cid=101010100&l=zh-CHS&p=CMA&a=0&u=C&s=3&m=1&x=1&d=3&fc=&bgc=&bc=&ti=1&in=1&li=2&ct=iframe' frameborder='0' scrolling='no' width='500' height='110' allowTransparency='true'></iframe><br>";
		}
		if ("地图".equals(keyword)) {
			tool = "<iframe width='425' height='350' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=zh&amp;geocode=&amp;q=%E4%B8%AD%E5%9B%BD%E5%B1%B1%E4%B8%9C%E7%9C%81&amp;aq=0&amp;oq=%E5%B1%B1%E4%B8%9C&amp;sll=37.0625,-95.677068&amp;sspn=38.41771,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Shandong,+China&amp;t=m&amp;z=6&amp;ll=36.668627,117.020411&amp;output=embed'></iframe><br /><small><a href='https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=%E4%B8%AD%E5%9B%BD%E5%B1%B1%E4%B8%9C%E7%9C%81&amp;aq=0&amp;oq=%E5%B1%B1%E4%B8%9C&amp;sll=37.0625,-95.677068&amp;sspn=38.41771,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Shandong,+China&amp;t=m&amp;z=6&amp;ll=36.668627,117.020411' style='color:#0000FF;text-align:left'>查看大图</a></small><br>";

		}
		if ("火车".equals(keyword)) {
			tool = "<IFRAME border=0 name=play  marginWidth=0 marginHeight=0 src=http://www1.xise.cn/che/ci.htm frameBorder=0 width=140 scrolling=no height=110></IFRAME><br>";

		}
		if ("时间".equals(keyword)) {
			tool = "<span id='clock'></span><script>setInterval('clock()',50);"
					+ "function clock(){document.getElementById('clock').innerHTML=new Date().toLocaleString();}</script> <a target='_blank' href='time.html'>展开万年历</a><br>";
		}
		if ("翻译".equals(keyword)) {
			tool = "<font color='red'><a href='fanyi.html' target='_blank'>打开MiracleSearch智能翻译</a></font><br>";

		}
		ServletActionContext.getRequest().setAttribute("expand", tool);
	}

	public Set<String> getHanzi() {
		return hanzi;
	}

	public void setHanzi(Set<String> hanzi) {
		this.hanzi = hanzi;
	}
}
