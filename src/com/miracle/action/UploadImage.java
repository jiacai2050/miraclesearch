package com.miracle.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


import com.miracle.util.Constants;
import com.opensymphony.xwork2.ActionSupport;

public class UploadImage extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private File upload;

	private String uploadContentType;

	private String uploadFileName;

	private String savePath;

	@Override
	public String execute() throws Exception {
		savePath = Constants.MIRACLESEARCH;
		File f = new File(getSavePath() + "upload/" + getUploadFileName());
		if (!f.exists()) {
			f.createNewFile();
		}
		
		FileOutputStream fos = new FileOutputStream(f);
		FileInputStream fis = new FileInputStream(getUpload());

		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = fis.read(buffer)) > 0) {
			fos.write(buffer, 0, len);
		}
		fis.close();
		fos.close();  //文件流必须关闭，不然会有内存泄漏
//		System.out.println(uploadFileName);
		return SUCCESS;
	}

	
	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

}
