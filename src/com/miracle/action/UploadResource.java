/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.miracle.action;

import java.io.*;
import java.net.InetAddress;

import com.miracle.util.Constants;
import com.miracle.util.ResourceAdd;
import com.opensymphony.xwork2.ActionSupport;
public class UploadResource extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private File upload;
	private String fileName;
	private String uploadtype;
	private String kind;
	private String describe;
	private String keywords;
	private String date;
	private String url;
	private String author;
	private String publisher;

	public String getUploadFileName() {
		return fileName;
	}

	public void setUploadFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUploadtype() {
		return uploadtype;
	}

	public void setUploadtype(String uploadtype) {
		this.uploadtype = uploadtype;
	}

	public String getDate() {
		return date;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public File getUpload() {
		return upload;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}


	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getUrl() {
		return url;
	}

	public String getPublisher() {
		return publisher;
	}

	public String getKind() {
		return kind;
	}

	public String getKeywords() {
		return keywords;
	}

	public String getDescribe() {
		return describe;
	}

	public String getAuthor() {
		return author;
	}

	public String execute() throws Exception {

		this.kind = fileName.substring(fileName.lastIndexOf(".") + 1);
		String title = fileName.substring(0, fileName.length() - kind.length()
				- 1);
		this.date = (new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))//24小时制
				.format(new java.util.Date());

		url = "http://" + InetAddress.getLocalHost().getHostAddress()
				+ ":8080/resources/" + fileName;

		ResourceAdd res = new ResourceAdd(title, kind, describe, keywords, date, url, author, publisher);
		if (res.process()) {
			System.out.println("test.xml更新成功");
		} else {
			System.out.println("test.xml更新失败");
		}
		java.io.InputStream in;
		in = new java.io.FileInputStream(upload);
		File file = new File(Constants.WEBAPP + "resources/", fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		java.io.OutputStream out = new java.io.FileOutputStream(file);
		byte[] buffer = new byte[1024];
		int count = 0;
		while ((count = in.read(buffer)) > 0) {
			out.write(buffer, 0, count);
		}
		in.close();
		out.close();

		return SUCCESS;
	}
}
