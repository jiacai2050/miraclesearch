package com.miracle.analyzer;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.wltea.analyzer.lucene.IKTokenizer;

public class MiracleAnalyzer extends Analyzer{

	
	private boolean useSmart;
	public MiracleAnalyzer(boolean useSmart) {//useSmart为true时使用智能模式
		this.useSmart = useSmart;
	}
	public MiracleAnalyzer() { // 默认为使用IKAnalyzer的细粒度策略
		this.useSmart = false;
	}
	
	@Override
	public TokenStream tokenStream(String fieldName, Reader reader) {
		
		return new MiracleTokenFilter(new IKTokenizer(reader, useSmart));
	}
	
	
}
