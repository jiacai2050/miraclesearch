package com.miracle.analyzer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.util.AttributeSource;

import com.miracle.util.Constants;
import com.miracle.util.PinyinUtil;

public class MiracleTokenFilter extends TokenFilter {

	private CharTermAttribute cta;
	private PositionIncrementAttribute pia;
	private AttributeSource.State current = null;

	private Stack<String> s = null;
	private List<String> list1 = null;
	private List<String> list2 = null;
	public MiracleTokenFilter(TokenStream input) {
		super(input);
		cta = this.addAttribute(CharTermAttribute.class);
		pia = this.addAttribute(PositionIncrementAttribute.class);
		s = new Stack<String>();
		
		list1 = new ArrayList<String>();
		list2 = new ArrayList<String>();
		wordProcess();
}
	public void wordProcess() {
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File(Constants.MIRACLESEARCH,"/stub/synonym.txt"))));
			
			String str = br.readLine();
			String s[] = new String[2];
			while(str != null) {
				s = str.split(",");
				if (s.length > 1) {

					list1.add(s[0]);
					list2.add(s[1]);	
				}
				str = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean incrementToken() throws IOException {

		if (s.size() > 0) {
			restoreState(current);
			cta.setEmpty();
			cta.append(s.pop());
			// 设置位置0
			pia.setPositionIncrement(0);
			return true;
		}
		if (!this.input.incrementToken())
			return false;
		current = captureState();
		getPinYin(cta.toString());
		return true;
	}
	public void getPinYin(String key) {
// if (key.length() > 1) { // 如果语汇单元是个词组，把拼音首字母也加入索引
// s.push(PinyinUtil.getFullPinYin(key));
// s.push(PinyinUtil.getFirstPinYin(key));
// } else {
// s.push(PinyinUtil.getFullPinYin(key));
// }
		s.push(PinyinUtil.getFullPinYin(key));
		getSynonym(key);

	}
	public void getSynonym(String key) {
		if (list1.contains(key)) {
			s.push(list2.get(list1.indexOf(key)));
		}
		if (list2.contains(key)) {
			s.push(list1.get(list2.indexOf(key)));
		}
	}
}
