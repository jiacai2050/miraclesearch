package com.miracle.bean;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.miracle.rmi.FileDecompression;

public class FileDecompressionBean {

	private String ip;
	private String file;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public FileDecompressionBean(String ip, String file) {
		super();
		this.ip = ip;
		this.file = file;
	}

	public FileDecompressionBean() {
		super();
	}

	public List<String> work() {
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		System.setProperty("java.security.policy", FileDecompressionBean.class
				.getResource("client.policy").toString());

		System.setSecurityManager(new RMISecurityManager());
		
		try {
			Context namingContext = new InitialContext();
			FileDecompression fd = (FileDecompression) namingContext.lookup("rmi://" + ip + ":1099/FileDecom");
			return fd.work(file);
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
}
