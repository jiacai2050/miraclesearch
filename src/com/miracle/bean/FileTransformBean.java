package com.miracle.bean;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.miracle.rmi.FileTransform;

public class FileTransformBean {

	private String ip;

	public boolean videoTrans(String source, String dest) {
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		System.setProperty("java.security.policy", FileTransformBean.class
				.getResource("client.policy").toString());

		System.setSecurityManager(new RMISecurityManager());

		try {
			Context namingContext = new InitialContext();

			String url = "rmi://" + ip;
			FileTransform ft = (FileTransform) namingContext.lookup(url
					+ ":1099/Transform");
			return ft.videoTransform(source, dest);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return false;

	}
	public boolean musicTrans(String source, String dest) {
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		System.setProperty("java.security.policy", FileTransformBean.class
				.getResource("client.policy").toString());

		System.setSecurityManager(new RMISecurityManager());

		try {
			Context namingContext = new InitialContext();

			String url = "rmi://" + ip;
			FileTransform ft = (FileTransform) namingContext.lookup(url
					+ ":1099/Transform");
			return ft.musicTransform(source, dest);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return false;

	}

	public boolean docTrans(String source, String pdf, String swf) {
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		System.setProperty("java.security.policy", FileTransformBean.class
				.getResource("client.policy").toString());

		System.setSecurityManager(new RMISecurityManager());

		try {
			Context namingContext = new InitialContext();

			String url = "rmi://" + ip;
			FileTransform ft = (FileTransform) namingContext.lookup(url
					+ ":1099/Transform");
			return ft.DocTransform(source, pdf, swf);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public FileTransformBean(String ip) {
		super();
		this.ip = ip;
	}

}
