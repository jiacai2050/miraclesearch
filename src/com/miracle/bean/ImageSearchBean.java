package com.miracle.bean;

import java.io.File;
import java.net.InetAddress;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.miracle.rmi.ImageTransport;
import com.miracle.util.Constants;
import com.miracle.util.ImageClient;

public class ImageSearchBean {

	private List<String> result;
	private long time;
	private List<String> pageList;
	private int length;

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public List<String> getPageList() {
		return pageList;
	}

	public void setPageList(List<String> pageList) {
		this.pageList = pageList;
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public ImageSearchBean() {
		result = new ArrayList<String>();

	}

	public List<String> doImageSearch(String imagePath, int pageIndex,
			String fileName) {
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		System.setProperty("java.security.policy", ImageSearchBean.class
				.getResource("client.policy").toString());

		System.setSecurityManager(new RMISecurityManager());

		try {
			Context namingContext = new InitialContext();
			List<String> list = getIPList(Constants.MIRACLESEARCH);

			List<String> tmp = new ArrayList<String>();

			long begin = System.currentTimeMillis();
			for (int i = 0; i < list.size() - 1; i++) {
				String url = "rmi://";
				url = url.concat(list.get(i + 1));
				try {
					ImageTransport imageTransport = (ImageTransport) namingContext
							.lookup(url + ":1099/ImageTransport");
					if (!InetAddress.getLocalHost().getHostAddress()
							.equalsIgnoreCase(list.get(i + 1))) {
						new ImageClient(list.get(i + 1), imagePath);//向其他服务器传送图片
					}
					
					List<String> l = imageTransport
							.advancedImageSearch(imagePath);// 注意这里的取值！！！
					if (l != null) {
						tmp.addAll(l);
					}

				} catch (NamingException e) {
					System.out.println(url + ":1099/Searchable没找到");
					e.printStackTrace();
				} catch (RemoteException e) {
					System.out.println("远程方法调用出错了");
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} // for list.size() over

			long end = System.currentTimeMillis();
			setTime((end - begin));
			setLength(tmp.size());
			int start = Integer.valueOf(pageIndex);
			int over = (start * 5 > tmp.size()) ? tmp.size() : (start * 5);
			for (int i = (start - 1) * 5; i < over; i++) {
				result.add(tmp.get(i));
			}// for over
			pageInit(getLength(), imagePath.substring(imagePath
					.lastIndexOf("/") + 1));
			return result;

		} catch (NamingException e) {
			System.out.println("namingContext初始化出错了");
			e.printStackTrace();
		}

		return null;
	}

	public void pageInit(int length, String fileName) {
		pageList = new ArrayList<String>();
		int totalPage = 0;
		if (length % 5 == 0) {
			totalPage = length / 5;
		} else {
			totalPage = length / 5 + 1;
		}
		for (int i = 0; i < totalPage; i++) {
			pageList.add("<a class='pages' href='ImageSearch?fileName="
					+ fileName + "&pageIndex=" + (i + 1) + "'>" + (i + 1)
					+ "</a>");
		}
	}

	@SuppressWarnings("unchecked")
	private List<String> getIPList(String path) {

		List<String> list = new ArrayList<String>();
		SAXReader reader = new SAXReader();
		try {
			File f = new File(path + "ip.xml");

			org.dom4j.Document doc = reader.read(f);
			Element root = doc.getRootElement();

			list.add(root.attributeValue("number"));
			for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
				Element e = it.next();
				list.add(e.getTextTrim());
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		return list;
	}
}
