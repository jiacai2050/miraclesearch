package com.miracle.bean;

import java.io.File;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.miracle.rmi.RemoteSearchable;
import com.miracle.util.Result;

public class ResultBean {

	private List<Result> resultList; // 搜索结果

	private long time; // 搜索用的时间

	private int length = 0; // 搜索结果的数目

	private List<String> pageList;

	private List<Result>[] l;

	@SuppressWarnings("unchecked")
	public boolean doRMISearch(String path, String keyword, int pageIndex,
			String type) {
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		System.setProperty("java.security.policy", ResultBean.class
				.getResource("client.policy").toString());

		System.setSecurityManager(new RMISecurityManager());

		List<Result> tmp = new ArrayList<Result>();
		try {
			Context namingContext = new InitialContext();
			List<String> list = getIPList(path);

			l = new ArrayList[Integer.valueOf(list.get(0))];
			long start = System.currentTimeMillis();
			for (int i = 0; i < list.size() - 1; i++) {
				String url = "rmi://";
				url = url.concat(list.get(i + 1));
				try {
					System.out.println(url + ":1099/Searchable");
					RemoteSearchable remoteSearchable = (RemoteSearchable) namingContext
							.lookup(url + ":1099/Searchable");
					l[i] = remoteSearchable.getResultList(path, keyword,
							pageIndex, type);
					length += remoteSearchable.getLength();
				} catch (NamingException e) {
					System.out.println(url + ":1099/Searchable没找到");
					e.printStackTrace();
				} catch (RemoteException e) {
					System.out.println("远程方法调用出错了");
					e.printStackTrace();
				}
			} // for list.size() over
			long end = System.currentTimeMillis();

			setTime((end - start)); // search time
			for (int i = 0; i < l[0].size(); i++) {
				for (int j = 0; j < l.length; j++) {
					if ( i < l[j].size()) {  // 防止不同服务器上的资源数目不相等
						tmp.add(l[j].get(i));
					}
					
				}
			}
		} catch (NamingException e) {
			System.out.println("namingContext初始化出错了");
			e.printStackTrace();
		}

		int start = Integer.valueOf(pageIndex);
		int over = (start * 10 > tmp.size()) ? tmp.size() : (start * 10);
		for (int i = (start - 1) * 10; i < over; i++) {
			resultList.add(tmp.get(i));
		}// for over
		pageInit(keyword, type);
		if (getLength() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public List<Result> getResultList() {
		return resultList;
	}

	public void setResultList(List<Result> resultList) {
		this.resultList = resultList;
	}

	public List<String> getPageList() {
		return pageList;
	}

	public void setPageList(List<String> pageList) {
		this.pageList = pageList;
	}

	public ResultBean() {
		resultList = new ArrayList<Result>();
		pageList = new ArrayList<String>();
	}

	@SuppressWarnings("unchecked")
	private List<String> getIPList(String path) {

		List<String> list = new ArrayList<String>();
		SAXReader reader = new SAXReader();
		try {
			File f = new File(path + "ip.xml");

			org.dom4j.Document doc = reader.read(f);
			Element root = doc.getRootElement();

			list.add(root.attributeValue("number"));
			for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
				Element e = it.next();
				list.add(e.getTextTrim());
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		return list;
	}

	public void pageInit(String keyword, String type) {
		int totalPage = 0;
		if (length % 10 == 0) {
			totalPage = length / 10;
		} else {
			totalPage = length / 10 + 1;
		}
		for (int i = 0; i < totalPage; i++) {
			pageList.add("<a class='pages' href=Search?pageIndex=" + (i + 1) + "&keyword="
					+ keyword + "&type=" + type + ">" + (i + 1) + "</a>");
		}
	}
//	public String getPageRange(String jspName, int pageNo,
//			int totalPage, int pageSize) {
//		int startPageNo = (pageNo - 1) / pageSize * pageSize;
//		int endPageNo = startPageNo + pageSize;
//		StringBuffer buf = new StringBuffer();
//		if (startPageNo > 0)
//			buf.append("<a href=" + jspName + "?pageNo="
//					+ (startPageNo - pageSize + 1) + ">[<<<]</a>");
//		if (totalPage >= pageSize) {
//			if (totalPage >= endPageNo)
//				for (int i = 1; i <= pageSize; ++i)
//					buf.append("<a href=" + jspName + "?pageNo="
//							+ (startPageNo + i) + ">[" + (startPageNo + i)
//							+ "]</a>&nbsp;&nbsp;");
//			else
//				for (int i = 1; i <= totalPage - startPageNo; ++i)
//					buf.append("<a href=" + jspName + "?pageNo="
//							+ (startPageNo + i) + ">[" + (startPageNo + i)
//							+ "]</a>&nbsp;&nbsp;");
//
//		} else
//			for (int i = 1; i <= totalPage; ++i)
//				buf.append("<a href=" + jspName + "?pageNo="
//						+ (startPageNo + i) + ">[" + (startPageNo + i)
//						+ "]</a>&nbsp;&nbsp;");
//
//		if (endPageNo != totalPage && endPageNo < totalPage)
//			buf.append("<a href=" + jspName + "?pageNo=" + (endPageNo + 1)
//					+ ">[>>>]</a>");
//		buf.append("&nbsp;&nbsp;&nbsp;Page No." + pageNo + "&nbsp;&nbsp;Total:"
//				+ totalPage);
//		return buf.toString();
//	}
}
