package com.miracle.heartbeat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.miracle.util.Constants;

public class HeartBeatReceive implements Runnable {

	private Map<String, Long> map;

	public static final int PORT = 5099;
	public static final String GROUP_IP = "239.1.1.10";
	private MulticastSocket socket;
	private InetAddress ia;

	//private String path;
	public HeartBeatReceive() {
		map = new HashMap<String, Long>();

		//this.path = path;
		try {
			this.ia = InetAddress.getByName(GROUP_IP);

			this.socket = new MulticastSocket(new InetSocketAddress(InetAddress
					.getLocalHost(), PORT));
			socket.joinGroup(ia);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("multisocket创建失败");
			e.printStackTrace();
		}
	}

	private void recevie() {
		byte[] data = new byte[10];
		DatagramPacket packet = new DatagramPacket(data, data.length);
		while (true) {
			try {
				socket.receive(packet);
				String address = packet.getAddress().getHostAddress();
				
				System.out.println("HeartBeatThread from " + address);

				Long time = System.currentTimeMillis();

				map.put(address, time);
				
				heartBeatCheck(time);

			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "multicast error");
				System.exit(1);
			}

		}
	}// receive

	public void heartBeatCheck(Long time) {

		Collection<Long> coll = map.values();
		List<String>list = new ArrayList<String>();
		for (Iterator<Long> iterator = coll.iterator(); iterator.hasNext();) {
			Long t = iterator.next();

			if ((time - t) > 5000) { // 超过5秒钟没有心跳
				for (String key : map.keySet()) {
					if (map.get(key).equals(t)) {// 找到相应的ip
						ipDelete(key);
						//map.remove(key);
						list.add(key);
						System.out.println("ip删除成功了");
					}
				}
				
			}
		}
		for (String string : list) {
			map.remove(string);
		}


	}

	@SuppressWarnings("unchecked")
	private void ipDelete(String ip) {
		File fip = new File(Constants.MIRACLESEARCH + "ip.xml");

		SAXReader reader = new SAXReader();
		Document doc = null;
		try {       
			doc = reader.read(fip);
 
		} catch (DocumentException e) {
			System.out.println( "读取ip.xml文件失败");
			e.printStackTrace();
		}
		Element root = doc.getRootElement();

		String old = root.attributeValue("number");
		root.attribute("number").setValue(
				Integer.valueOf(Integer.valueOf(old) - 1).toString());// number减1

		for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
			Element element = it.next();
			if (ip.equalsIgnoreCase(element.getTextTrim())) // 在ip.xml中查找所对应的ip
				root.remove(element);
				System.out.println(ip + "移除成功了");
		}
		
		try {
			XMLWriter output = new XMLWriter(new FileWriter(fip));
			output.write(doc);// 保存文档
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 

	}

	public void run() {
		this.recevie();
	}
}
