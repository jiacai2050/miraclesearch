package com.miracle.heartbeat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;

public class HeartBeatThr implements Runnable{

	public final int PORT = 5099;
	public final String GROUP_IP = "239.1.1.10";
	private MulticastSocket socket;
	private InetAddress ia;

//	private String path;
	
	public HeartBeatThr() {
		try {
			ia = InetAddress.getByName(GROUP_IP);
			socket = new MulticastSocket(new InetSocketAddress(InetAddress
					.getLocalHost(), PORT));
			socket.joinGroup(ia);
		} catch (IOException e) {
			System.out.println("socket创建失败");
			e.printStackTrace();
		}
		
		
	}
	private void send() {

		String data = "ok";
		byte[] b = data.getBytes();
		DatagramPacket packet = new DatagramPacket(b, b.length, ia, PORT);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}// send
	public void run() {
		while(true) {
			try {
				Thread.sleep(4000);//每隔5秒“心跳”一次
			} catch (InterruptedException e) {
				System.out.println("线程睡眠失败 ");
				e.printStackTrace();
			}
			this.send();
			System.out.println("心跳监测数据发送成功");
		}
		
	}

}
