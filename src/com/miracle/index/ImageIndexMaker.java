package com.miracle.index;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.DocumentBuilderFactory;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.miracle.util.Constants;


public class ImageIndexMaker {

	public ImageIndexMaker() {
	}

	public boolean doIndex() {
		File file = new File(Constants.WEBAPP + "resources/");
		List<File> list = new ArrayList<File>();

		for (File image : file.listFiles()) {
			if (image.getName().endsWith("jpg")
					|| image.getName().endsWith("bmp")) {
				list.add(image);
			}
		}// for

		DocumentBuilder builder = DocumentBuilderFactory
				.getDefaultDocumentBuilder();
		try {
			Directory directory = FSDirectory.open(new File(
					Constants.MIRACLESEARCH + "/image_index"));
			IKAnalyzer ik = new IKAnalyzer();
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36,
					ik);
			config.setOpenMode(OpenMode.CREATE); // 每次重启tomcat都新建索引
			IndexWriter iw = new IndexWriter(directory, config);

			for (File image : list) {
				Document doc = builder.createDocument(
						new FileInputStream(image), "http://"
								+ InetAddress.getLocalHost().getHostAddress()
								+ ":8080/resources/" + image.getName());
				iw.addDocument(doc);
			}
			iw.close();
			System.out.println("图片索引创建完毕");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// public static void main(String[] args) {
	// String path = "D:/Program
	// Files/apache-tomcat-6.0.16/webapps/MiracleSearch";
	// new ImageIndexMaker(path).doIndex();
	// }
}
