package com.miracle.index;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.miracle.analyzer.MiracleAnalyzer;
import com.miracle.util.AllTextGet;
import com.miracle.util.XmlReader;

public class IndexMaker {
	public static boolean doCreate(File index) {
		try {
			if (!index.exists()) {
				index.mkdir();
			}
			FSDirectory fs = FSDirectory.open(index);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36,
					new MiracleAnalyzer()); // 使用我们重写的分词器
			config.setOpenMode(OpenMode.CREATE); // 每次重启tomcat都新建索引
			IndexWriter iwriter = new IndexWriter(fs, config);
			XmlReader xmlreader = new XmlReader();
			ArrayList<ArrayList<String>> list = xmlreader.parse();
			long begin = System.currentTimeMillis();
			AllTextGet textGetHelper = new AllTextGet();
			for (int i = 0; i < list.size(); i++) {
				ArrayList<String> l = list.get(i);
				Document doc = new Document();
				doc.add(new Field("id", l.get(0), Store.YES, Index.NO));
				doc
						.add(new Field("title", l.get(1), Store.YES,
								Index.ANALYZED));
				doc.add(new Field("keywords", l.get(2), Store.YES,
						Index.ANALYZED));
				doc.add(new Field("kind", l.get(3), Store.YES, Index.NO));
				doc.add(new Field("describe", l.get(4), Store.YES,
						Index.ANALYZED));
				doc.add(new Field("date", l.get(5), Store.YES, Index.NO));
				doc.add(new Field("url", l.get(6), Store.YES, Index.NO));
				doc
						.add(new Field("author", l.get(7), Store.YES,
								Index.ANALYZED));
				doc.add(new Field("publisher", l.get(8), Store.YES,
						Index.ANALYZED));
				doc.add(new Field("content", textGetHelper.doWork(l.get(1)
						+ "." + l.get(3)), Store.YES, Index.ANALYZED));
				iwriter.addDocument(doc);
			}
			iwriter.close();
			fs.close();
			long end = System.currentTimeMillis();
			System.out.println("建索引一共耗时" + (begin - end) + "ms");
			return true;
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (LockObtainFailedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
