package com.miracle.index;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.miracle.bean.ResultBean;
import com.miracle.heartbeat.HeartBeatReceive;
import com.miracle.heartbeat.HeartBeatThr;
import com.miracle.multisocket.SendAndReceive;
import com.miracle.rmi.Server;
import com.miracle.util.AppStart;
import com.miracle.util.Constants;
import com.miracle.util.ImageServer;

@SuppressWarnings("serial")
public class Init extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	public void init() throws ServletException {
		Constants.MIRACLESEARCH = getServletContext().getRealPath("/");

		Constants.WEBAPP = Constants.MIRACLESEARCH.replaceAll(
				"MiracleSearch.$", "");

		// System.exit(0);
		File file = new File(Constants.MIRACLESEARCH, "index");
		
		IndexMaker.doCreate(file);
		
		File image = new File(Constants.MIRACLESEARCH, "image_index");
		if (!image.exists()) {
			ImageIndexMaker imageIndex = new ImageIndexMaker();
			imageIndex.doIndex(); // 为图片高级搜索建立索引
		}

		File ipFile = new File(Constants.MIRACLESEARCH + "ip.xml");
		if (ipFile.exists()) {
			ipFile.delete();
		}

		File upload = new File(Constants.MIRACLESEARCH, "upload");
		if (!upload.exists()) {
			upload.mkdir();
		}
		File rar = new File(Constants.MIRACLESEARCH, "rar");
		if (!rar.exists()) {
			rar.mkdir();
		}

		SendAndReceive sar = new SendAndReceive();
		Thread t = new Thread(sar);
		t.start();
		System.out.println("组播启动成功");

		try {
			Server.registryObject();
			new ResultBean().doRMISearch(Constants.MIRACLESEARCH, "测试", 1,
					"all");
		} catch (Exception e) {
			e.printStackTrace();
		}

		AppStart.openOfficeStart();

		new Thread(new ImageServer()).start();

		 new Thread(new HeartBeatReceive()).start();
		 System.out.println("心跳包检测启动成功");
				
		 new Thread(new HeartBeatThr()).start();
		 System.out.println("心跳包启动成功");

	}

}
