package com.miracle.multisocket;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.miracle.util.Constants;

public class SendAndReceive implements Runnable {

	public static final int PORT = 4099;
	public static final String GROUP_IP = "239.1.1.111";
	private MulticastSocket socket;
	private InetAddress ia;

	private SendAndReceive sar;
	
	public SendAndReceive() {
		try {
			ia = InetAddress.getByName(GROUP_IP);
			socket = new MulticastSocket(new InetSocketAddress(InetAddress
					.getLocalHost(), PORT));
			// System.out.println(socket.getLocalAddress().getHostAddress());
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}// Construction

	public void send() {

		String data = "online";
		byte[] b = data.getBytes();
		DatagramPacket packet = new DatagramPacket(b, b.length, ia, PORT);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}// send

	@SuppressWarnings("unchecked")
	public void ipWriter(String remoteIp) {
		File fip = new File(Constants.MIRACLESEARCH + "ip.xml");
		if (!fip.exists()) {
			try {
				fip.createNewFile();
				Document document = DocumentHelper.createDocument();
				Element ipList = document.addElement("ipList");
				ipList.addAttribute("number", "0");
				ipList.setText("");
				XMLWriter output = new XMLWriter(new FileWriter(fip)); // 保存文档
				output.write(document);
				output.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		try {

			SAXReader reader = new SAXReader();
			Document doc = reader.read(fip);
			Element root = doc.getRootElement();

			for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
				Element element = it.next();
				if (remoteIp.equalsIgnoreCase(element.getTextTrim())) // 如果列表中有当前的ip就直接返回
					return;
			}

			String old = root.attributeValue("number");
			root.attribute("number").setValue(
					Integer.valueOf(Integer.valueOf(old) + 1).toString());// number
			// =
			// number
			// + 1;
			root.addElement("ip").setText(remoteIp);

			XMLWriter output = new XMLWriter(new FileWriter(fip)); // 保存文档
			output.write(doc);
			output.close();
			System.out.println("ip更新成功了");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}// ipWriter

	public void receive() {
		byte[] data = new byte[512];
		DatagramPacket packet = new DatagramPacket(data, data.length);
		while (true) {
			try {
				socket.receive(packet);
				String receive = new String(data, 0, packet.getLength());
				String address = packet.getAddress().getHostAddress();
				System.out.println(receive + " from " + address);

				
				
				ipWriter(packet.getAddress().getHostAddress());
				
				if (!"known".equals(receive)) {

					String str = "known"; // 告诉新加入的socket，我知道你加入了
					DatagramPacket sendp = new DatagramPacket(str.getBytes(),
							str.length(), ia, PORT);
					socket.send(sendp);

				} 
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "multicast error");
				System.exit(1);
			}

		}
	}// receive


	public void join() {
		try {
			socket.joinGroup(ia);
			this.send();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void leave() {
		try {
			socket.leaveGroup(ia);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("我要走了");
		}
	}

	public void run() {

		sar = new SendAndReceive();
		sar.join();
		sar.receive();

	}
	

}
