package com.miracle.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface FileDecompression extends Remote{

	List<String> work(String FilePath) throws RemoteException;
}
