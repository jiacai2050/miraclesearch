package com.miracle.rmi;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import com.miracle.util.Constants;

public class FileDecompressionImpl extends UnicastRemoteObject implements
		FileDecompression {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> list;

	protected FileDecompressionImpl() throws RemoteException {
		super();
		
	}

	public List<String> work(String fileName) throws RemoteException {
		try {

			String cmd = Constants.WEBAPP + "/WinRAR/WinRAR.exe";

			String path = "\"" + Constants.WEBAPP + "resources/" + fileName
					+ "\"";
			File file = new File(Constants.MIRACLESEARCH + "rar", fileName
					.substring(0, fileName.lastIndexOf(".")));
			if (!file.exists()) {
				file.mkdir();
			}
			String unrarCmd = cmd + " x -r -p- -o+ " + path + " "
					+ ("\"" + file.getAbsolutePath() + "\"");

			Runtime rt = Runtime.getRuntime();
			Process pre = rt.exec(unrarCmd);
			InputStreamReader isr = new InputStreamReader(pre.getInputStream());
			BufferedReader bf = new BufferedReader(isr);
			String line = null;
			while ((line = bf.readLine()) != null) {
				line = line.trim();
				if ("".equals(line)) {
					continue;
				}
			}

			bf.close();

			showAllFiles(file);

			return this.list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void showAllFiles(File dir) throws Exception {
		list = new ArrayList<String>();
		File[] fs = dir.listFiles();
		for (int i = 0; i < fs.length; i++) {
			if (fs[i].isDirectory()) {
				try {
					showAllFiles(fs[i]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				list.add(fs[i].getAbsolutePath());
			}

		}
	}
	// public static void main(String[] args) throws Exception{
	// FileDecompressionImpl fdi = new FileDecompressionImpl();
	// fdi.showAllFiles(new File("D:/test"));
	// for(String str : fdi.list) {
	// System.out.println(str);
	// }
	// }
	// public static void main(String[] args) {
	// String targetPath = "D:/test";
	// String rarFilePath = "D:/test.zip";
	// FileDecompressionImpl unrar = new FileDecompressionImpl();
	// unrar.unRarFile(targetPath, rarFilePath);
	// }
}
