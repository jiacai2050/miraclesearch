package com.miracle.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FileTransform extends Remote {

	public boolean DocTransform(String sourceFile, String pdfFile, String swfFile) throws RemoteException;
	
	public boolean videoTransform(String sourceVideo, String destVideo) throws RemoteException;
	
	public boolean musicTransform(String source, String dest) throws RemoteException;
}
