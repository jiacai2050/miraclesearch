package com.miracle.rmi;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import com.miracle.util.Constants;
import com.miracle.util.Doc2swf;

public class FileTransformImpl extends UnicastRemoteObject implements
		FileTransform {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileTransformImpl() throws RemoteException {
		super();
	}

	public boolean DocTransform(String sourceFile, String pdfFile,
			String swfFile) throws RemoteException {
		return new Doc2swf(sourceFile, pdfFile, swfFile).doWork();
	}

	public boolean videoTransform(String sourceVideo, String destVideo) {
		List<String> commend = new ArrayList<String>();
		commend.add(Constants.WEBAPP + "ffmpeg.exe");
		commend.add("-i");
		commend.add("\"" + sourceVideo + "\"");
		commend.add("-ab");
		commend.add("56");
		commend.add("-ar");
		commend.add("22050");
		commend.add("-b");
		commend.add("230");
		commend.add("-vb");
		commend.add("800");
//		commend.add("-t");
//		commend.add("30");
		commend.add("-r"); // 设定帧率
		commend.add("29.97");
		commend.add("\"" + destVideo + "\"");

		ProcessBuilder builder = new ProcessBuilder();
		builder.command(commend);
		try {
			builder.start();// 开始转化视频
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean musicTransform(String source, String dest) {
		List<String> commend = new ArrayList<String>();
		commend.add(Constants.WEBAPP + "ffmpeg.exe");
		commend.add("-i");
		commend.add("\"" + source + "\"");
		commend.add("\"" + dest + "\"");

		ProcessBuilder builder = new ProcessBuilder();
		builder.command(commend);
		try {
			builder.start();// 开始转化视频
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
