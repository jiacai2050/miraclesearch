package com.miracle.rmi;


import java.rmi.Remote;
import java.util.List;

public interface ImageTransport extends Remote
{
//	byte[] image2Bytes(String imagePath) throws Exception;
	List<String> advancedImageSearch(String fileName) throws Exception;
}

