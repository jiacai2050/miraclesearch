package com.miracle.rmi;

import java.io.File;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;


import com.miracle.util.Constants;
import com.miracle.util.ImageSearch;

public class ImageTransportImpl extends UnicastRemoteObject implements
		ImageTransport {

	private static final long serialVersionUID = 1L;

	public ImageTransportImpl() throws Exception {
		super();

	}

	// public byte[] image2Bytes(String imagePath) {
	// this.imagePath = imagePath;
	// try {
	// BufferedImage bimg = ImageIO.read(new File(imagePath));
	// ByteArrayOutputStream imageStream = new ByteArrayOutputStream();
	//
	// ImageIO.write(bimg, imagePath.substring(imagePath.lastIndexOf(".")+1),
	// imageStream);
	// return imageStream.toByteArray();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	public List<String> advancedImageSearch(String imagePath) throws Exception {
		String file = Constants.MIRACLESEARCH + "/upload/"
				+ new File(imagePath).getName();
		int times = 0;
		while (!new File(file).exists()) {
			Thread.sleep(10);
			times++;
			if (times > 2000) {
				return null;
			}
		}
		ImageSearch is = new ImageSearch(Constants.MIRACLESEARCH
				+ "image_index", file);
		return is.lireSearch();
	}
}