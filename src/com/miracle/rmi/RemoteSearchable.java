package com.miracle.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import com.miracle.util.Result;


public interface RemoteSearchable extends Remote {

	public int getLength() throws RemoteException;
	public List<Result> getResultList(String ipListPath, String keyword, int pageIndex,
			String type) throws RemoteException;
	
}
