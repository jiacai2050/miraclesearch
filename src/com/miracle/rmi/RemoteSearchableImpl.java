package com.miracle.rmi;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.miracle.analyzer.MiracleAnalyzer;
import com.miracle.util.Result;
import com.miracle.util.TypeSort;

public class RemoteSearchableImpl extends UnicastRemoteObject implements
		RemoteSearchable {

	private static final long serialVersionUID = 1L;
	private List<Result> resultList;
	private int length;

	public RemoteSearchableImpl() throws RemoteException {
		super();
	}

	public List<Result> getResultList(String path, String keyword,
			int pageIndex, String type) throws RemoteException {
		IndexReader ir = null;
		IndexSearcher is = null;
		try {
			Directory directory = FSDirectory.open(new File(path + "/index"));

			ir = IndexReader.open(directory);

			is = new IndexSearcher(ir);

			MiracleAnalyzer analyzer = new MiracleAnalyzer();

			String[] queries = { keyword, keyword, keyword, keyword };
			String[] fields = { "title", "keywords", "describe", "content" };
			BooleanClause.Occur[] clauses = { BooleanClause.Occur.SHOULD,
					BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD,
					BooleanClause.Occur.SHOULD };

			long t3 = System.currentTimeMillis();
			Query query = MultiFieldQueryParser.parse(Version.LUCENE_36,
					queries, fields, clauses, analyzer);
			long t4 = System.currentTimeMillis();
			System.out.println("�ִ���ʱ" + (t4 - t3));

			TopDocs top = is.search(query, 1000);

			ScoreDoc[] hits = top.scoreDocs;

			resultList = new ArrayList<Result>();
			TypeSort ts = new TypeSort(resultList, hits, is);

			if ("all".equals(type)) {
				ts.doAll(query);
			} else if ("document".equals(type)) {
				ts.doDocument(query);
			} else if ("image".equals(type)) {
				ts.doImage(query);
			} else if ("music".equals(type)) {
				ts.doMusic(query);
			} else if ("video".equals(type)) {
				ts.doVideo(query);
			} else {
				ts.doOther(query);
			}
			this.setLength(resultList.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ir != null) {
					ir.close();
				}
				if (is != null) {
					is.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return this.resultList;

	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getLength() throws RemoteException {
		return length;
	}

}
