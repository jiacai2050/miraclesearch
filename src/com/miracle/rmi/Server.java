package com.miracle.rmi;

import java.rmi.registry.LocateRegistry;

import javax.naming.Context;
import javax.naming.InitialContext;

public class Server {

	public static void registryObject() throws Exception {

		RemoteSearchableImpl impl = new RemoteSearchableImpl();
		FileTransformImpl impl2 = new FileTransformImpl();
		ImageTransportImpl iti = new ImageTransportImpl();	
		FileDecompressionImpl fdi = new FileDecompressionImpl();
		System.setProperty("java.rmi.server.codebase",
				"http://localhost:8080/MiracleSearch/stub/");

		LocateRegistry.createRegistry(1099);

		Context namingContext = new InitialContext();
		namingContext.rebind("rmi:Searchable", impl);
		namingContext.rebind("rmi:Transform", impl2);
		namingContext.rebind("rmi:ImageTransport", iti);
		namingContext.rebind("rmi:FileDecom", fdi);
	}

}
