package com.miracle.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

import org.apache.poi.POITextExtractor;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

public class AllTextGet {

	// public static void main(String[] args) {
	// AllTextGet getText = new AllTextGet();
	// System.out.println(getText.pdfReader("D:/计算机.pdf"));
	// System.out.println("-------");
	//		
	// }
	public String doWork(String fileName) {
		fileName = Constants.WEBAPP + "resources/" + fileName;
		
		if (fileName.endsWith("doc") || fileName.endsWith("docx")) {
			return WordReader(fileName);
		} else if (fileName.endsWith("ppt") || fileName.endsWith("pptx")) {
			return pptReader(fileName);
		} else if (fileName.endsWith("xls") || fileName.endsWith("xlsx")) {
			return pptReader(fileName);
		} else if (fileName.endsWith("pdf")) {
			return pdfReader(fileName);
		} else if (fileName.endsWith("txt")) {
			return txtReader(fileName);
		} else {
			return "";
		}
	}

	// EXCEL中处理日期和数字
	private String getRightStr(String sNum) {
		DecimalFormat decimalFormat = new DecimalFormat("#.000000");
		String resultStr = decimalFormat.format(new Double(sNum));
		if (resultStr.matches("^[-+]?\\d+\\.[0]+$")) {
			resultStr = resultStr.substring(0, resultStr.indexOf("."));
		}
		return resultStr;
	}

	// 读取EXCEL文本
	public String ExcelReader(String fileName) {

		int totalRows = 0; // 总行数
		int totalCells = 0; // 总列数
		int numOfSheets = 0; // Sheet数量
		int currSheet = 0;// 当前Sheet数量
		Sheet sheet = null;
		String filetype;
		InputStream inputStream = null;
		Workbook wb = null;
		String excelText = "";

		if (!new File(fileName).exists()) {
			System.out.println(fileName + "不存在");
			return "";
		}

		filetype = fileName.substring(fileName.lastIndexOf(".") + 1);

		try {
			inputStream = new FileInputStream(fileName);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}// 创建文件输入流
		try {
			if (filetype.equalsIgnoreCase("xls")) {
				wb = new HSSFWorkbook(inputStream); // 如果是2003版本
			} else if (filetype.equalsIgnoreCase("xlsx")) {
				wb = new XSSFWorkbook(inputStream); // 如果是2007版本
			}
			totalRows = 0;
			totalCells = 0;
			currSheet = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}

		numOfSheets = wb.getNumberOfSheets(); // 取得当前sheet数目

		while (currSheet < numOfSheets) {
			sheet = wb.getSheetAt(currSheet); // 根据当前sheet获取sheet
			totalRows = sheet.getPhysicalNumberOfRows();
			// dataLst = new ArrayList<ArrayList<String>>();
			if (totalRows >= 1 && sheet.getRow(0) != null) {
				totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
			}
			for (int r = 0; r < totalRows; r++) {
				Row row = sheet.getRow(r);
				if (row == null) {
					continue;
				}

				// ArrayList<String> rowLst = new ArrayList<String>();
				for (short c = 0; c < totalCells; c++) {
					Cell cell = row.getCell(c);
					String cellValue = "";
					if (cell == null) {
						continue;
					}
					// 处理数字型的,自动去零
					if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
						// 在excel里,日期也是数字,在此要进行判断 */
						if (DateUtil.isCellDateFormatted(cell)) {
							Date date = cell.getDateCellValue();
							cellValue = DateFormat.getDateInstance().format(
									date);
						} else {
							cellValue = getRightStr(cell.getNumericCellValue()
									+ "");
						}
					}
					// 处理字符串型
					else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
						cellValue = cell.getStringCellValue();
					}
					// 处理布尔型
					else if (Cell.CELL_TYPE_BOOLEAN == cell.getCellType()) {
						cellValue = cell.getBooleanCellValue() + "";
					}
					// 其它的,非以上几种数据类型
					else {
						cellValue = cell.toString() + "";
					}
					excelText += (cellValue + " ");
				}
			}
			currSheet++;
		}
		return excelText;
	}

	// 读取WORD文本
	public String WordReader(String fileName) {
		String fileType;
		WordExtractor ex = null;
		String text = "";
		OPCPackage opcPackage = null;
		POIXMLTextExtractor extractor = null;

		if (!new File(fileName).exists()) {
			System.out.println(fileName + "不存在");
			return "";
		}

		fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
		try {
			if (fileType.equalsIgnoreCase("doc")) {
				ex = new WordExtractor(new FileInputStream(new File(fileName))); // 如果是2003版本word
			} else if (fileType.equalsIgnoreCase("docx")) {
				opcPackage = POIXMLDocument.openPackage(fileName); // 如果是2007版本word
			}
		} catch (IOException e) {
			System.out.println(fileName);
			e.printStackTrace();
		}

		if (fileType.equalsIgnoreCase("doc")) {
			text = ex.getText(); // 如果是2003版本word
		} else if (fileType.equalsIgnoreCase("docx")) {
			try {
				extractor = new XWPFWordExtractor(opcPackage); // 如果是2007版本word
				text = extractor.getText();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			text = "";
		}
		return text;

	}

	// 读取PPT文本
	public String pptReader(String fileName) {
		POITextExtractor extractor = null;

		if (!new File(fileName).exists()) {
			System.out.println(fileName + "不存在");
			return "";
		}
		String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
		try {
			if (fileType.equalsIgnoreCase("ppt")
					|| fileType.equalsIgnoreCase("pptx")) {
				extractor = ExtractorFactory
						.createExtractor(new File(fileName));
				return extractor.getText();
			} else {
				return "";
			}
		} catch (Exception e) {
			System.out.println(fileName);
			e.printStackTrace();
		}
		return "";
	}

	// 读取PDF文本
	public String pdfReader(String file) {
		if (!new File(file).exists()) {
			return "";
		}
		boolean sort = false;
		String pdfFile = file;
		String textFile = null;
		String encoding = "gbk";
		int startPage = 1;
		int endPage = Integer.MAX_VALUE;
		Writer output = null;
		PDDocument document = null;
		try {
			try {
				// 首先当作一个URL来装载文件，如果得到异常再从本地文件系统//去装载文件
				URL url = new URL(pdfFile);
				document = PDDocument.load(url);
				String fileName = url.getFile();
				// 以原来PDF的名称来命名新产生的txt文件
				if (fileName.length() > 4) {
					File outputFile = new File(fileName.substring(0, fileName
							.length() - 4)
							+ ".txt");
					textFile = outputFile.getName();
				}
			} catch (MalformedURLException e) {
				// 如果作为URL装载得到异常则从文件系统装载
				try {
					document = PDDocument.load(pdfFile);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (pdfFile.length() > 4) {
					textFile = pdfFile.substring(0, pdfFile.length() - 4)
							+ ".txt";
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			output = new OutputStreamWriter(new FileOutputStream(textFile),
					encoding);
			PDFTextStripper stripper = null;
			stripper = new PDFTextStripper();
			stripper.setSortByPosition(sort);
			stripper.setStartPage(startPage);
			stripper.setEndPage(endPage);
			stripper.writeText(document, output);
			return txtReader(textFile);
		} catch (UnsupportedEncodingException e) {
			System.out.println(file);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println(file);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(file);
			e.printStackTrace();
		} finally {
			try {
				if (output != null) {
					output.close();
				}
				if (document != null) {
					document.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	// 读取txt文本
	public String txtReader(String fileName) {
		if (!new File(fileName).exists()) {
			return "";
		}

		// 从生成的TEXT中把内容读出来
		File f = new File(fileName);
		String line = "";
		String text = "";
		try {
			InputStreamReader isr = new InputStreamReader(new FileInputStream(f
					.getPath()));
			BufferedReader br = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				if (line.length() != 0) {
					text = text + line;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}

}