package com.miracle.util;

import java.io.IOException;
import java.util.List;

public class AppStart {

	public static void openOfficeStart() {
		List<String> commend = new java.util.ArrayList<String>();
		commend.add(Constants.WEBAPP + "OpenOffice/program/soffice.exe");
		commend.add("-headless");
		commend.add("-accept=socket,host=127.0.0.1,port=8100;urp;");
		commend.add("-nofirststartwizard");// 设定比特率

		ProcessBuilder builder = new ProcessBuilder();
		builder.command(commend);
		// Process processOpenOffice = builder.start(); // 执行转换
		try {
			builder.start();
		} catch (IOException e) {
			e.printStackTrace();
		} // 执行转换
		System.out.println("openOffice服务开启了");

	}
}
