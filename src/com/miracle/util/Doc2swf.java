package com.miracle.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.List;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;

public class Doc2swf implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String source;
	private String pdf;
	private String swf;

	public Doc2swf(String source, String pdf, String swf) {
		super();
		this.source = source;
		this.pdf = pdf;
		this.swf = swf;
	}

	public Doc2swf() {
		super();
	}
	public boolean doWork() {
		if (source.endsWith("pdf")) {
			return pdf2swf();
		} else {
			return doc2pdf();			
		}
	}
	public boolean doc2pdf() {
		File sourceFile = new File(source);
		File pdfFile = new File(pdf);
		if (sourceFile.exists()) {
			if (!pdfFile.exists()) {

				try {

					OpenOfficeConnection connection = new SocketOpenOfficeConnection(
							"127.0.0.1", 8100);
					connection.connect();

					// convert
					DocumentConverter converter = new OpenOfficeDocumentConverter(
							connection);
					converter.convert(sourceFile, pdfFile);

					// close the connection
					connection.disconnect();

				} catch (java.net.ConnectException e) {
					e.printStackTrace();
					System.out.println("服务启动未成功！");
					return false;
				} catch (com.artofsolving.jodconverter.openoffice.connection.OpenOfficeException e) {
					e.printStackTrace();
					System.out.println("读取文件失败！");
					return false;
				} catch (Exception e) {
					e.printStackTrace();
				} 
			} else {
				System.out.println("pdf已存在，无需再次转换！");
			}
		} else {
			System.out.println("所要转换的文件不存在！");
			return false;
		}
		return pdf2swf();
	}

	public boolean pdf2swf() {
		File pdfFile = new File(pdf);
		File swfFile = new File(swf);
		if (!swfFile.exists()) {
			if (pdfFile.exists()) {
				Process swfToolsPro = null;
				try {
					List<String> commend = new java.util.ArrayList<String>();
					commend.add(Constants.WEBAPP + "SWFTools/pdf2swf.exe");

					commend.add("-o");
					commend.add(swfFile.toString());
					commend.add("-s");// 设定比特率
					commend.add("flashversion=9");
					commend.add(pdfFile.toString());// 设定声道
					ProcessBuilder builder = new ProcessBuilder();
					builder.command(commend);
					swfToolsPro = builder.start(); // 执行转换

					BufferedReader br = new BufferedReader(
							new InputStreamReader(swfToolsPro.getInputStream()));
					try {
						while (br.readLine() != null)
							// 清空缓存
							;
					} catch (IOException e) {
						e.printStackTrace();
					}

					swfToolsPro.waitFor();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (swfToolsPro != null) {
						swfToolsPro.destroy();
					}
				}
			} else {
				System.out.println("pdf不存在！");
				return false;
			}
		} else {
			System.out.println("swf已存在，无需再次转换！");
		}
		return true;
	}
}
