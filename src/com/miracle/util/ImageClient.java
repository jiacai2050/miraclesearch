package com.miracle.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.Socket;

import javax.imageio.ImageIO;

public class ImageClient {
	public ImageClient(String ip, String fileName) {
		try {
			Socket s = new Socket(ip, 8889);

			DataOutputStream dout = new DataOutputStream(s.getOutputStream());
			dout.writeUTF(fileName);
			dout.flush();
			if (!(fileName.endsWith("jpg")|| fileName.endsWith("bmp") || fileName.endsWith("gif"))) {
				return;
			}
			BufferedImage image = ImageIO.read(new File(fileName)); // ��ȡ1.gif������
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(image,
					fileName.substring(fileName.lastIndexOf(".") + 1), out);
			
			byte[] b = out.toByteArray();
			dout.write(b);
			out.close();
			s.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
