package com.miracle.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.ImageSearcherFactory;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;

public class ImageSearch {

	private String indexPath;
	private String searchFile;

	public ImageSearch(String indexPath, String searchFile) {
		this.indexPath = indexPath;
		this.searchFile = searchFile;
	}

	public ImageSearch() {
		super();
	}

	public List<String> lireSearch() {
		List<String> list = new ArrayList<String>();
		try {
			IndexReader reader = IndexReader.open(FSDirectory.open(new File(
					indexPath)));

			ImageSearcher searcher = ImageSearcherFactory
					.createDefaultSearcher();

			FileInputStream imageStream = new FileInputStream(searchFile);
			BufferedImage bimg = ImageIO.read(imageStream);

			// Search for similar images
			ImageSearchHits hits = searcher.search(bimg, reader);

			for (int i = 0; i < hits.length(); i++) {
				if (hits.score(i) > 0.7) {
					list.add(hits.doc(i).get(DocumentBuilder.FIELD_NAME_IDENTIFIER));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
//	public static void main(String[] args) {
//		String path = "D:/Program Files/apache-tomcat-6.0.16/webapps/MiracleSearch/image_index";
//		List<String> list = new ImageSearch(path, "D:/123.jpg").lireSearch();
//		
//		for (String string : list) {
//			System.out.println(string);
//		}
//	}
	
	
	
	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}

	public String getSearchFile() {
		return searchFile;
	}

	public void setSearchFile(String searchFile) {
		this.searchFile = searchFile;
	}

}
