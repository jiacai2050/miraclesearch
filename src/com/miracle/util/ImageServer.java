package com.miracle.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.imageio.ImageIO;

public class ImageServer implements Runnable {

	private ServerSocket server;

	public ImageServer() {
		try {
			server = new ServerSocket(8889);
			System.out.println("服务器开启连接...端口为8889");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			while (true) {
				Socket s = server.accept();
				System.out.println("一客户端连接服务器，服务器接受图片...");

				DataInputStream in = new DataInputStream(s.getInputStream());
				String imagePath = in.readUTF();
//				System.out.println(imagePath);
				byte[] b = new byte[1024];
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				int length = 0;
				while ((length = in.read(b)) != -1) {
					bout.write(b, 0, length);
				}
				ByteArrayInputStream bin = new ByteArrayInputStream(bout
						.toByteArray());
				BufferedImage image = ImageIO.read(bin);
				ImageIO.write(image, imagePath.substring(imagePath
						.lastIndexOf(".") + 1), new File(
						Constants.MIRACLESEARCH + "upload/"
								+ new File(imagePath).getName()));

				System.out.println("图片接受完毕");
				bin.close();
				bout.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
