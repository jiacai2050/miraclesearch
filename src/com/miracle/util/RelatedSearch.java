package com.miracle.util;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TextFragment;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.miracle.analyzer.MiracleAnalyzer;

public class RelatedSearch {

	public List<String> doRelateSearch(String keyword) {

		try {
			Directory directory = FSDirectory.open(new File(
					Constants.MIRACLESEARCH, "index"));
			IndexReader indexReader = IndexReader.open(directory);

			IndexSearcher indexSearcher = new IndexSearcher(indexReader);

			FuzzyQuery fuzzyQuery = new FuzzyQuery(new Term("title", keyword),
					0.1f);
			TopDocs top = indexSearcher.search(fuzzyQuery, 5);
			List<String> list = new ArrayList<String>();
			ScoreDoc[] hits = top.scoreDocs;
			for (int i = 0; i < hits.length; i++) {
				Document doc = indexSearcher.doc(hits[i].doc);
				list.add("<a href=Search.action?keyword=" + doc.get("title")
						+ "&type=all>" + turnRed(fuzzyQuery, doc, "title")
						+ "</a>");
				// System.out.println(turnRed(fuzzyQuery, doc, "title"));
			}
			indexSearcher.close();
			indexReader.clone();
			return list;
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String turnRed(Query query, Document doc, String field)
			throws Exception {
		SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter(
				"<font color=\"red\">", "</font>");
		Highlighter highlighter = new Highlighter(htmlFormatter,
				new QueryScorer(query));
		MiracleAnalyzer analyzer = new MiracleAnalyzer(false);

		String text = doc.get(field);
		TokenStream tokenStream = analyzer.tokenStream(field, new StringReader(
				doc.get(field)));
		TextFragment[] frag = highlighter.getBestTextFragments(tokenStream,
				text, false, 10);// highlighter.getBestFragments(tokenStream,
		// text, 3, "...");
		for (int j = 0; j < frag.length; j++) {
			if ((frag[j] != null) && (frag[j].getScore() > 0)) {
				return (frag[j].toString());
			}
		}
		return text;
	}

	public Set<String> hanziSuggestion(String keyword, String type) {
		Set<String> set = new HashSet<String>();

		try {
			Directory directory = FSDirectory.open(new File(Constants.MIRACLESEARCH, "index"));
			IndexReader indexReader = IndexReader.open(directory);

			IndexSearcher indexSearcher = new IndexSearcher(indexReader);

			Term term = new Term("title", keyword);
			TermQuery query = new TermQuery(term);

			TopDocs docs = indexSearcher.search(query, 100);

			ScoreDoc[] hits = docs.scoreDocs;
			Pattern p = Pattern.compile("<font color=\"red\">\\W{0,5}</font>");
			for (int i = 0; i < hits.length; i++) {
				Document doc = indexReader.document(hits[i].doc);
				Matcher m = p.matcher(turnRed(query, doc, "title"));
				while (m.find()) {
					// System.out.println(m.group());
					String tmp = m.group();
					tmp = tmp.replaceAll("<font color=\"red\">", "");
					tmp = tmp.replaceAll("</font>", "");
					// set.add(tmp);
					set.add("<a href=\"Search.action?keyword=" + tmp + "&type="
							+ type + "&pageIndex=1\">" + m.group() + "</a>");
				}

			}

		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return set;
	}

	// public static void main(String[] args) {
	// Set<String>set = new RelatedSearch().hanziSuggestion("sx");
	// for (String string : set) {
	// System.out.println(string);
	// }
	//		
	// }
}
