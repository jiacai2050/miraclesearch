package com.miracle.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class ResourceAdd {

	private String title;
	private String kind;
	private String describe;
	private String keywords;
	private String date;
	private String url;
	private String author;
	private String publisher;

	
	public ResourceAdd(String title, String kind, String describe,
			String keywords, String date, String url, String author,
			String publisher) {
		this.title = title;
		this.kind = kind;
		this.describe = describe;
		this.keywords = keywords;
		this.date = date;
		this.url = url;
		this.author = author;
		this.publisher = publisher;
	}

	@SuppressWarnings("unchecked")
	public boolean process() {

		try {

			File xmlPath = new File(Constants.WEBAPP, "test.xml");
			SAXReader reader = new SAXReader();
			Document document = reader.read(xmlPath);
			Element root = document.getRootElement();

			int index = 0;
			for (Iterator i = root.elementIterator(); i.hasNext(); i.next()) {
				index++;
			}
			int id = index + 1;
			Element rs = root.addElement("resourceitem");

			Element idElement = rs.addElement("id");
			Element titleElement = rs.addElement("title");
			Element keywordsElementt = rs.addElement("keywords");
			Element kindElement = rs.addElement("kind");
			Element describeElement = rs.addElement("describe");
			Element dateElement = rs.addElement("date");
			Element urlElement = rs.addElement("url");
			Element authorElement = rs.addElement("author");
			Element publisherElement = rs.addElement("publisher");

//			System.out.println("title: " + title);
//			System.out.println("keywords: " + keywords);
//			System.out.println("kind: " + kind);
//			System.out.println("describe: " + describe);
//			System.out.println("date: " + date);
//			System.out.println("url: " + url);
//			System.out.println("author: " + author);
//			System.out.println("publisher: " + publisher);

			/** 为title设置内容 */
			idElement.setText(String.valueOf(id));
			titleElement.setText(title);
			/** 为keywords设置内容 */
			keywordsElementt.setText(keywords);
			/** 为kind设置内容 */
			kindElement.setText(kind);
			/** 为describe设置内容 */
			describeElement.setText(describe);
			/** 为date设置内容 */
			dateElement.setText(date);
			/** 为url设置内容 */
			urlElement.setText(url);
			/** 为author设置内容 */
			authorElement.setText(author);
			/** 为publisher设置内容 */
			publisherElement.setText(publisher);

			document.setRootElement(root);
			/** 格式化输出，类型IE浏览一样 */
			OutputFormat format = OutputFormat.createPrettyPrint();
			/** 指定XML编码 */
			format.setEncoding("UTF-8");
			XMLWriter writer = new XMLWriter(new FileOutputStream(xmlPath),
					format);
			writer.write(document);
			writer.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

}
