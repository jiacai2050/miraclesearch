package com.miracle.util;

import java.io.Serializable;


//定义搜索的结果集
public class Result implements Serializable{

	private static final long serialVersionUID = 5828206090986565406L;

	private String title;
	private String kind;
	private String describe;
	private String date;
	private String url;
	private String author;
	private String publisher;
	private String download;
	private String content;
	
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDownload() {
		return download;
	}
	public void setDownload(String download) {
		this.download = download;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Result(String title, String kind, String describe, String date,
			String url, String author, String publisher, String download,String content) {
		super();
		this.title = title;
		this.kind = kind;
		this.describe = describe;
		this.date = date;
		this.url = url;
		this.author = author;
		this.publisher = publisher;
		this.download = download;
		this.content = content;
	}
	public Result() {
		super();
	}
	
	
}