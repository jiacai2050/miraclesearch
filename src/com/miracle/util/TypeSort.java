package com.miracle.util;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TextFragment;

import com.miracle.analyzer.MiracleAnalyzer;

public class TypeSort {

	private List<Result> resultList;

	private ScoreDoc[] hits;

	private IndexSearcher is;

	public TypeSort(List<Result> resultList, ScoreDoc[] hits, IndexSearcher is) {
		super();
		this.resultList = resultList;
		this.hits = hits;
		this.is = is;
	}

	public TypeSort() {
		super();
	}

	public void doAll(Query query) {
		for (int i = 0; i < hits.length; i++) {
			try {
				Document hitDoc = is.doc(hits[i].doc);
				Result result = new Result();

				pushResult(result, hitDoc, query);
			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void doDocument(Query query) {
		for (int i = 0; i < hits.length; i++) {
			try {
				Document hitDoc = is.doc(hits[i].doc);
				Result result = new Result();

				if ("doc".equals(hitDoc.get("kind"))
						|| "ppt".equals(hitDoc.get("kind"))
						|| "xls".equals(hitDoc.get("kind"))
						|| "pdf".equals(hitDoc.get("kind"))) {

					pushResult(result, hitDoc, query);

				}

			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void doImage(Query query) {
		for (int i = 0; i < hits.length; i++) {
			try {
				Document hitDoc = is.doc(hits[i].doc);
				Result result = new Result();

				if ("jpg".equals(hitDoc.get("kind"))
						|| "bmp".equals(hitDoc.get("kind"))
						|| "gif".equals(hitDoc.get("kind"))
						|| "png".equals(hitDoc.get("kind"))) {

					pushResult(result, hitDoc, query);

				}
			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void doMusic(Query query) {
		for (int i = 0; i < hits.length; i++) {
			try {
				Document hitDoc = is.doc(hits[i].doc);
				Result result = new Result();

				if ("mp3".equals(hitDoc.get("kind"))
						|| "wma".equals(hitDoc.get("kind"))) {

					pushResult(result, hitDoc, query);

				}
			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void doVideo(Query query) {
		for (int i = 0; i < hits.length; i++) {
			try {
				Document hitDoc = is.doc(hits[i].doc);
				Result result = new Result();

				if ("rm".equals(hitDoc.get("kind"))
						|| "rmvb".equals(hitDoc.get("kind"))
						|| "avi".equals(hitDoc.get("kind"))
						|| "swf".equals(hitDoc.get("kind"))) {

					pushResult(result, hitDoc, query);

				}
			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void doOther(Query query) {
		for (int i = 0; i < hits.length; i++) {
			try {
				Document hitDoc = is.doc(hits[i].doc);
				Result result = new Result();

				if ("rar".equals(hitDoc.get("kind"))
						|| "zip".equals(hitDoc.get("kind"))
						|| "xls".equals(hitDoc.get("kind"))
						|| "pdf".equals(hitDoc.get("kind"))) {

					pushResult(result, hitDoc, query);

				}
			} catch (CorruptIndexException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void pushResult(Result result, Document hitDoc, Query query) {
		// String url = hitDoc.get("url");
		// url = url.substring(7, url.indexOf('/', 7)); // 202.194.127.182:8080
		String url = hitDoc.get("url");
		String title = "";
		try {
			if ("swf".equals(hitDoc.get("kind"))) {
				title = "<a target='_blank' href="
						+ hitDoc.get("url") // swf动画可以在浏览器直接显示
						+ ">" + this.turnRed(query, hitDoc, "title") + "</a>";

			} else {
				title = "<a target='_blank' href=Preview.action?fileName="
					+ java.net.URLEncoder.encode(url, "UTF-8") // 解决ie8中URL传参乱码问题
					+ "&id=" + hitDoc.get("id") + ">"
					+ this.turnRed(query, hitDoc, "title") + "</a>";
				
			}
			result.setTitle(title);
			result.setKind(hitDoc.get("kind"));
			result.setDescribe(this.turnRed(query, hitDoc, "describe"));
			result.setDate(hitDoc.get("date"));
			result.setUrl(hitDoc.get("url"));
			result.setAuthor(hitDoc.get("author"));
			result.setPublisher(hitDoc.get("publisher"));
			String content =this.turnRed(query, hitDoc, "content");
			//String content = hitDoc.get("content");
			if (content.length() > 150)
				content = content.substring(0, 149);
			result.setContent(content);
			String regEx = "\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}";
			Pattern p = Pattern.compile(regEx);
			Matcher m = p.matcher(url);
			String ip = null;
			if (m.find()) {
				ip = m.group();
			}

			result.setDownload("http://" + ip
					+ ":8080/MiracleSearch/Download.action?fileName="
					+ hitDoc.get("title") + "." + hitDoc.get("kind"));

			resultList.add(result); // 加入到result集合里面
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// private String turnRed(Query query, Document doc, String field)
	// throws IOException, InvalidTokenOffsetsException {
	// IKAnalyzer analyzer = new IKAnalyzer();
	// SimpleHTMLFormatter simpleHtmlFormatter = new SimpleHTMLFormatter(
	// "<font color=\"red\">", "</font>");
	// Highlighter highlighter = new Highlighter(simpleHtmlFormatter,
	// new QueryScorer(query));
	// TokenStream tokenStream = analyzer.tokenStream(field, new StringReader(
	// doc.get(field)));
	// String highlighterStr = highlighter.getBestFragment(tokenStream, doc
	// .get(field));
	// return highlighterStr == null ? doc.get(field) : highlighterStr;
	// }
	private String turnRed(Query query, Document doc, String field)
			throws Exception {
		SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter(
				"<font color=\"red\">", "</font>");
		Highlighter highlighter = new Highlighter(htmlFormatter,
				new QueryScorer(query));
		MiracleAnalyzer analyzer = new MiracleAnalyzer(false);

		String text = doc.get(field);
		TokenStream tokenStream = analyzer.tokenStream(field, new StringReader(
				doc.get(field)));
		TextFragment[] frag = highlighter.getBestTextFragments(tokenStream,
				text, false, 10);// highlighter.getBestFragments(tokenStream,
		// text, 3, "...");
		for (int j = 0; j < frag.length; j++) {
			if ((frag[j] != null) && (frag[j].getScore() > 0)) {
				return (frag[j].toString());
			}
		}
		return text;
	}

	public List<Result> getResultList() {
		return resultList;
	}

	public void setResultList(List<Result> resultList) {
		this.resultList = resultList;
	}

	public ScoreDoc[] getHits() {
		return hits;
	}

	public void setHits(ScoreDoc[] hits) {
		this.hits = hits;
	}

	public IndexSearcher getIs() {
		return is;
	}

	public void setIs(IndexSearcher is) {
		this.is = is;
	}

}
