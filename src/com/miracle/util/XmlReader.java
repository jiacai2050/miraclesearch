package com.miracle.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XmlReader {

	//private String xmlPath;
	private Document document;

	public XmlReader() {
		//this.xmlPath = xmlPath;
		SAXReader reader = new SAXReader();
		try {
			this.document = reader.read(new File(Constants.WEBAPP, "test.xml"));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public ArrayList<ArrayList<String>> parse() {
		Element root = this.document.getRootElement();
		List<Element> list = root.elements();
		ArrayList<ArrayList<String>> l = new ArrayList<ArrayList<String>>();
		for(int i = 0; i < list.size(); i ++) {
			ArrayList<String> li = new ArrayList<String>();
			li.add(list.get(i).elementText("id"));
			li.add(list.get(i).elementText("title"));
			li.add(list.get(i).elementText("keywords"));	
			li.add(list.get(i).elementText("kind"));	
			li.add(list.get(i).elementText("describe"));	
			li.add(list.get(i).elementText("date"));	
			li.add(list.get(i).elementText("url"));	
			li.add(list.get(i).elementText("author"));	
			li.add(list.get(i).elementText("publisher"));	
			l.add(li);
		}
		return l;
	}

}